#!/usr/bin/php
<?php

$config = dirname(__FILE__) . '/protected/config/console.php';

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));

require_once dirname(__FILE__) . '/libs/vendor/yii/YiiBase.php';

class Yii extends YiiBase
{
    /**
     * Для автокомплита IDE.
     *
     * @static
     * @return MyWebApplication
     */
    public static function app()
    {
        return parent::app();
    }
}

if (isset($config)) {
    $app = Yii::createConsoleApplication($config);
    $app->getCommandRunner()->addCommands(YII_PATH . '/cli/commands');
    $env = @getenv('YII_CONSOLE_COMMANDS');
    if (!empty($env)) {
        $app->getCommandRunner()->addCommands($env);
    }
} else {
    $app = Yii::createConsoleApplication(array('basePath' => dirname(__FILE__) . '/cli'));
}

$app->run();
