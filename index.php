<?php
// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

$config = dirname(__FILE__) . '/protected/config/frontend.php';

require_once dirname(__FILE__) . '/libs/vendor/yii/YiiBase.php';
require_once dirname(__FILE__) . '/protected/components/MyWebApplication.php';

class Yii extends YiiBase {
    /**
     * Для автокомплита IDE.
     *
     * @static
     * @return MyWebApplication
     */
    public static function app()
    {
        return parent::app();
    }
}

Yii::createApplication('MyWebApplication', $config)->runEnd('frontend');
