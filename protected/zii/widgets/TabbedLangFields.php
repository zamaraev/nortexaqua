<?php
/**
 * Created by JetBrains PhpStorm.
 * User: accessd
 * Date: 15.08.12
 * Time: 16:34
 * Виджет для организации мультиязычных полей в группы по табам
 */
$GLOBALS['TABINDEX'] = 0; // Хак для использования нескольких TabView на странице TODO: может использовать вместо этого random?

class TabbedLangFields extends CTabView
{
    public $model;
    public $form;
    public $fields;

    public function init()
    {
        $auth = Yii::app()->authManager;
        $languages = array('_lang1','_lang2');

        foreach ($languages as $i => $lang) {
            $i++;
            $GLOBALS['TABINDEX'] = $GLOBALS['TABINDEX'] + $i;
            $lang_post_fix = "_lang$i";

                $this->tabs["tab" . $GLOBALS['TABINDEX']] = array(
                    'title' => "Поля ({$lang})",
                    'view' => "/shared/lang_fields",

                    'data' => array('model' => $this->model, 'form' => $this->form, 'fields' => $this->fields, 'field_postfix' => $lang_post_fix),
                );
        }
        parent::init();
    }
}
