<?php

Yii::import('zii.widgets.CMenu');

class MyMenu extends CMenu
{
    protected function renderMenuItem($item)
    {
        if (isset($item['count'])) {
            $item['label'] .= ' <span class="label">' . $item['count'] . '</span>';
        }

        return parent::renderMenuItem($item);
    }
}
