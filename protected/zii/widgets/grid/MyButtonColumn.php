<?php

class MyButtonColumn extends CButtonColumn
{
    public function init()
    {
        $template = array();

        $controller = Yii::app()->controller;
        $controllerActions = $controller->actions();

        if (key_exists('view', $controllerActions) || method_exists($controller, 'actionView')) {
            $template[] = '{view}';
        }
        if (key_exists('update', $controllerActions) || method_exists($controller, 'actionUpdate')) {
            $template[] = '{update}';
        }
        if (key_exists('delete', $controllerActions) || method_exists($controller, 'actionDelete')) {
            $template[] = '{delete}';
        }

        $this->template = implode(' ', $template);

        $urlParts = '';
        $controllerQuery = Yii::app()->getRequest()->getQuery($controller->model);
        if ($controllerQuery !== null) {
            foreach ($controllerQuery as $key => $value) {
                $urlParts .= ', "' . $controller->model . '[' . $key . ']" => "' . $value . '"';
            }
        }

        $this->viewButtonUrl = 'Yii::app()->controller->createUrl("view", array("id" => $data->primaryKey' . $urlParts . '))';
        $this->updateButtonUrl = 'Yii::app()->controller->createUrl("update", array("id" => $data->primaryKey' . $urlParts . '))';
        $this->deleteButtonUrl = 'Yii::app()->controller->createUrl("delete", array("id" => $data->primaryKey' . $urlParts . '))';

        parent::init();
    }
}
