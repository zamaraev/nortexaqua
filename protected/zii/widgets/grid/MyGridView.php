<?php

Yii::import('zii.widgets.grid.CGridView');
Yii::import('application.zii.widgets.grid.*');

class MyGridView extends CGridView
{
    public $showTableOnEmpty = true;
    public $ajaxUpdate = false;
    public $selectableRows = 1000;
    public $enableSorting = true;
    public $enablePagination = true;
    public $pager = array('class' => 'MyBackendLinkPager');
    public $pagerCssClass = 'pagination';
    public $summaryText = '{start}&mdash;{end} из {count}';
    public $template = "{items}\n{actions}\n{pager}";
    public $itemsCssClass = 'table';

    public $groupBy;
    public $groupByValue;

    public $sortable = false;
    public $sortableUrl = null;

    protected $lastGroupBy;

    public function init()
    {
        $basePath = dirname(__FILE__) . '/../assets/' . get_class($this);
        $this->baseScriptUrl = Yii::app()->getAssetManager()->publish($basePath);

        if ($this->sortable == true) {
            $this->dataProvider->pagination = false;
        }

        parent::init();
    }

    public function registerClientScript()
    {
        if ($this->sortableUrl === null) {
            $this->sortableUrl = Yii::app()->controller->createUrl('sort');
        }

        if ($this->sortable == true) {
            Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
            Yii::app()->getClientScript()->registerScript('grid-sortable', "
                $('#{$this->id}').sortable({
                    items: 'tbody > tr',
                    handle: 'img.sort',
                    update: function() {
                        $.ajax({
                            url: '" . $this->sortableUrl . "',
                            type: 'POST',
                            data: $(this).sortable('serialize')
                        });
                    }
                });
            ");
        }

        return parent::registerClientScript();
    }

    public function initColumns()
    {
        if ($this->sortable == true) {
            array_unshift($this->columns, array(
                'value' => "MyHtml::image(Yii::app()->getRequest()->baseUrl . '/images/icons/fugue/arrow-resize-090.png', '', array('class' => 'sort'))",
                'type' => 'raw',
                'htmlOptions' => array('width' => 10),
            ));
        }

        foreach ($this->columns as $i => $column) {
            if (is_string($column)) {
                $options = array();
                switch ($column) {
                    case 'id':
                        $options = array('headerHtmlOptions' => array('width' => 20));
                        break;

                    case 'created_at':
                    case 'updated_at':
                        $options = array('headerHtmlOptions' => array('width' => 150));
                        break;
                }
                $column = CMap::mergeArray(array('name' => $column), $options);
                $this->columns[$i] = $column;
            }
        }

        parent::initColumns();
    }

    public function renderTableRow($row)
    {
        $data = $this->dataProvider->data[$row];

        if (isset($this->groupBy)) {
            $groupBy = $this->groupBy;
            if (!isset($this->lastGroupBy) || $this->lastGroupBy != $data->$groupBy) {
                echo '<tr class="even">';
                echo '<td colspan="' . count($this->columns) . '"><div class="group">';
                $groupByText = $this->evaluateExpression($this->groupByValue, array('data' => $data));
                echo MyHtml::link($groupByText, array('products/admin', 'products[group_id]' => $data->group_id));
                echo '</div></td>';
                echo '</tr>';
                $this->lastGroupBy = $data->$groupBy;
            }
        }

        if (is_array($this->rowCssClass) && ($n = count($this->rowCssClass)) > 0) {
            echo '<tr class="' . $this->rowCssClass[$row % $n] . '" id="grid_item_' . $data->getPrimaryKey() . '">';
        } else {
            echo '<tr>';
        }

        foreach ($this->columns as $column) {
            $column->renderDataCell($row);
        }

        echo '</tr>';
    }

    public function renderActions()
    {
        if ($this->dataProvider->getItemCount() > 0) {
            $checkBoxColumn = null;
            foreach ($this->columns as $column) {
                if ($column instanceof MyCheckBoxColumn) {
                    $checkBoxColumn = $column;
                    break;
                }
            }

            if ($checkBoxColumn && !empty($checkBoxColumn->actions)) {
                echo MyHtml::form('', 'post', array('id' => 'form-actions'));
                echo '<div id="keys-actions" class="hidden2"></div>';
                echo '<table class="grid-actions" width="100%">';
                echo '<tr>';
                echo '<td>';
                echo 'С выделенными: ';
                echo MyHtml::dropDownList('action', null, $checkBoxColumn->actions, array('prompt' => '---'));
                echo '</td>';
                echo '</tr>';
                echo '</table>';
                echo MyHtml::endForm();

                Yii::app()->getClientScript()->registerScript('change-action', "
                    $('#action').bind('change', function() {
                        if ($(this).val() != '') {
                            var keysActions = $('#keys-actions');
                            var formActions = $('#form-actions');
                            var selectedKeys = $.fn.yiiGridView.getChecked('" . $this->id . "', '" . $checkBoxColumn->id . "');

                            if(selectedKeys.length > 0) {
                                keysActions.html('');
                                $.each(selectedKeys, function(k, v) {
                                    keysActions.append('<input type=\"hidden\" name=\"keys[]\" value=\"' + v + '\" />');
                                });
                                formActions.get(0).setAttribute('action', APP.baseRequestUrl + $(this).val());
                                formActions.submit();
                            } else {
                                $('#action option:first').attr('selected','selected');
                            }
                        }
                    });
                ");
            }
        }
    }

    public function renderEmptyText() {
        $emptyText = $this->emptyText === null ? Yii::t('zii','No results found.') : $this->emptyText;
        echo '<div class="alert">' . $emptyText . '</div>';
    }
}
