<?php

return array(
    'Id' => '#',
    'Status' => 'Статус',
    'Created at' => 'Дата создания',
    'Updated at' => 'Дата обновления',
    'Title' => 'Название',
    'Url' => 'Ссылка',
    'Picture' => 'Изображение',
    'Date' => 'Дата',
    'Body' => 'Текст',
);
