<?php

return array(
    'contact_form' => 'Обратная связь / Отзывы',
    'recent_news' => 'Контактная информация',
    'telephone' => 'Телефон горячей линии (звонок бесплатный)',
    'fax' => 'Факс',
    'clear' => 'Очистить',
    'send' => 'Отправить',
    'text'=>'По всем интересующим Вас вопросам а также узнать расположение дилеров нашей компании в Вашем регионе можете отправив нам запрос или позвонив на телефон горячей линии.',



);