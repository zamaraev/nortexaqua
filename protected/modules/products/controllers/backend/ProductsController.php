<?php

class ProductsController extends MyBackendController
{
    public $model = 'Products';

    public function actions()
    {
        return array(
            'index' => 'application.components.backend.actions.RedirectToAdmin',
            'admin' => 'application.components.backend.actions.Admin',
            'update' => 'application.components.backend.actions.Update',
            'create' => 'application.components.backend.actions.Create',
            'delete' => 'application.components.backend.actions.Delete',
        );
    }

    public function adminAttributes()
    {
        return array(
            array('id'),
            array(
                'name' => 'title',
            ),
            array('created_at'),
            array('updated_at'),
        );
    }
}
