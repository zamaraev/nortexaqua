<?php

/**
 * Класс модели для таблицы "News".
 *
 * @property integer $id
 * @property integer $status
 * @property double $price
 * @property double $received
 * @property string $created_at
 * @property string $updated_at
 *
 */
class Products extends MyActiveRecord
{


    /**
     * Правила.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('title, text,  created_at', 'required'),
            array('title, picture', 'length', 'max' => 255),
            array('updated_at, picture,picture1', 'safe'),
            array('picture, picture1', 'file', 'types' => 'jpeg,jpg,gif,png', 'allowEmpty' => true),
            array('id, text, title, created_at, updated_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Связи.
     *
     * @return array
     */
    public function relations()
    {
        return array(

        );
    }


    /**
     * @param string $className
     * @return self
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
