<?php

/** @var $form MyBackendActiveForm */
$form = $this->beginWidget('MyBackendActiveForm', array(
    'htmlOptions' => array('enctype' => 'multipart/form-data'),));

echo $form->beginForm();
echo $form->errorSummary($model);

echo $form->beginField();
echo $form->renderField($model, 'title');
echo $form->endField();

echo $form->beginField();
echo $form->renderField($model, 'text');
echo $form->endField();

echo $form->beginField();
echo $form->renderField($model, 'picture');
echo $form->endField();

echo $form->beginField();
echo $form->renderField($model, 'picture1');
echo $form->endField();


echo $form->buttons($model);
echo $form->endForm();

$this->endWidget();
