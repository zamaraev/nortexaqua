<?php

class LogsController extends SettingsBackendController
{
    public $model = 'Logs';

    public function actions()
    {
        return array(
            'index' => 'application.components.backend.actions.RedirectToAdmin',
            'admin' => 'application.components.backend.actions.Admin',
        );
    }

}
