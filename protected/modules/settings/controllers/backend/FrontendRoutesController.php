<?php

class FrontendRoutesController extends SettingsBackendController
{
    public $model = 'SettingsFrontendRoutes';

    public function actions()
    {
        return array(
            'index' => 'application.components.backend.actions.RedirectToAdmin',
            'admin' => 'application.components.backend.actions.Admin',
            'update' => 'application.components.backend.actions.Update',
            'create' => 'application.components.backend.actions.Create',
            'delete' => 'application.components.backend.actions.Delete',
        );
    }

    public function adminAttributes()
    {
        return array(
            array(
                'name' => 'path',
            ),
            array(
                'name' => 'route',
                'value' => 'MyHtml::frontendPathNameFromJson($data->route)',
                'type' => 'html',
            ),
            array('created_at'),
            array('updated_at'),
        );
    }
}
