<?php

class m121006_111527_create_settings extends EDbMigration
{
    private $_table = 'settings';

    public function safeUp()
    {
        $this->createTable($this->_table, array(
            'id' => 'pk',
            'data' => 'text',
        ), 'ENGINE InnoDB');
    }

    public function safeDown()
    {
        $this->dropTable($this->_table);
    }
}
