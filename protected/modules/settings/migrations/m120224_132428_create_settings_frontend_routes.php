<?php

class m120224_132428_create_settings_frontend_routes extends EDbMigration
{
    private $_table = 'settings_frontend_routes';

    public function safeUp()
    {
        $this->createTable($this->_table, array(
            'id' => 'pk',
            'path' => 'string NOT NULL',
            'route' => 'string NOT NULL',
            'created_at' => 'timestamp NOT NULL DEFAULT NOW()',
            'updated_at' => 'timestamp',
        ), 'ENGINE InnoDB');
    }

    public function safeDown()
    {
        $this->dropTable($this->_table);
    }
}
