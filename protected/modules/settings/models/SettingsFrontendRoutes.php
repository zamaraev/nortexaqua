<?php

class SettingsFrontendRoutes extends SettingsFrontendRoutesBase
{
    /**
     * @static
     * @param string $className
     * @return self
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array_merge(
            parent::rules(),
            array(
                array('path', 'unique'),
            )
        );
    }
}
