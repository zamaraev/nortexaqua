<?php

/**
 * Класс модели для таблицы "settings".
 *
 * @property integer $id
 * @property string $data
 *
 * @package Models
 * @author  Dmitriy Neshin <just.neshin@gmail.com>
 */
class SettingsBase extends MyActiveRecord
{
    /**
     * Правила.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('data', 'safe'),
            array('id, data', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Связи.
     *
     * @return array
     */
    public function relations()
    {
        return array(
        );
    }
}
