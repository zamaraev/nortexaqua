<?php

class Settings extends SettingsBase
{
    /**
     * @param string $className
     * @return self
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

//    public function __get($name)
//    {
//        return '';
//        parent::__get($name);
//    }

    public function findByConstant($constant) {
        $model = $this->findByPk($constant);
        if ($model == null) {
            $model = new self;
            $model->id = $constant;
        }
        return $model;
    }
}
