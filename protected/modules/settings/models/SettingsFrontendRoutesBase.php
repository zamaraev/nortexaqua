<?php

/**
 * Класс модели для таблицы "settings_frontend_routes".
 *
 * @property integer $id
 * @property string $path
 * @property string $route
 * @property string $created_at
 * @property string $updated_at
 *
 * @package Models
 * @author  Dmitriy Neshin <just.neshin@gmail.com>
 */
class SettingsFrontendRoutesBase extends MyActiveRecord
{
    /**
     * Правила.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('path, route, created_at', 'required'),
            array('path, route', 'length', 'max' => 255),
            array('updated_at', 'safe'),
            array('id, path, route, created_at, updated_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Связи.
     *
     * @return array
     */
    public function relations()
    {
        return array(
        );
    }
}
