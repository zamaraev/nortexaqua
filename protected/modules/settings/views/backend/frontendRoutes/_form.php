<?php

/** @var $form MyBackendActiveForm */
$form = $this->beginWidget('MyBackendActiveForm');

echo $form->beginForm();
echo $form->errorSummary($model);

echo $form->beginField();
echo $form->renderField($model, 'route');
echo $form->endField();

echo $form->beginField();
echo $form->myTextField($model, 'path', array('appendText' => '.html'));
echo $form->endField();

echo $form->buttons($model);
echo $form->endForm();

$this->endWidget();
