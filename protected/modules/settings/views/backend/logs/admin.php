<?php
$this->widget('application.zii.widgets.grid.MyGridView', array(
    'id' => 'bars-grid',
    'dataProvider' => $model->search(),
    'showTableOnEmpty' => true,
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id',
            'filter' => false,
        ),
        array(
            'name' => 'order_id',
        ),
        array(
            'name'=>'device_id',
            'value' => '$data->device->type_device',
            'header' => 'Устройство',
            'filter' => SettingsDevices::model()->findAllForFilterByLogs(),
        ),
        array(
            'name' => 'type',
            'filter' => false,
        ),
        array(
            'name' => 'description',
            'filter' => false,
        ),
        array(
            'name' => 'created_at',
            'filter' => false,
        ),
        array(
            'class' => 'MyButtonColumn',
        ),
    ),
));
