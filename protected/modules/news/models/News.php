<?php

/**
 * Класс модели для таблицы "News".
 *
 * @property integer $id
 * @property integer $status
 * @property double $price
 * @property double $received
 * @property string $created_at
 * @property string $updated_at
 *
 */
class News extends MyActiveRecord
{


    /**
     * Правила.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('text_lang1, text_lang2, title_lang1, title_lang2 ,created_at', 'required'),
            array('updated_at', 'safe'),
            array('id, text_lang1, text_lang2, title_lang1, title_lang2, created_at, updated_at, error_status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Связи.
     *
     * @return array
     */
    public function relations()
    {
        return array(

        );
    }

    /**
     * @param string $className
     * @return self
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
