<?php

class m130620_071822_news extends MyDbMigration
{
    private $_table = 'news';

    public function safeUp()
    {
        $this->createTable($this->_table, array(
            'id' => 'pk',
            'text' => 'text NOT NULL',
            'created_at' => 'timestamp NOT NULL DEFAULT NOW()',
            'updated_at' => 'timestamp',
        ), 'ENGINE InnoDB');
    }

    public function safeDown()
    {
        $this->dropTable($this->_table);
    }
}
