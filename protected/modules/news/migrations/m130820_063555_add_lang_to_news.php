<?php

class m130820_063555_add_lang_to_news extends MyDbMigration
{
    private $_table = 'news';

    public function safeUp()
    {
/*        $this->dropColumn($this->_table, 'title');
        $this->dropColumn($this->_table, 'text');

        $this->addColumn($this->_table, 'text_lang1', 'text');
        $this->addColumn($this->_table, 'text_lang2', 'text');
        $this->addColumn($this->_table, 'title_lang1', 'varchar(60)');
        $this->addColumn($this->_table, 'title_lang2', 'varchar(60)');*/
    }

    public function safeDown()
    {
        $this->dropColumn($this->_table, 'text_lang1');
        $this->dropColumn($this->_table, 'text_lang2');

        $this->dropColumn($this->_table, 'title_lang1');
        $this->dropColumn($this->_table, 'title_lang2');

    }
}
