<?php

class m130620_074458_add_news_title_colunt extends MyDbMigration
{
    private $_table = 'news';

    public function safeUp()
    {
        $this->addColumn($this->_table, 'title', 'varchar(60) NOT NULL');
    }

    public function safeDown()
    {
    }
}
