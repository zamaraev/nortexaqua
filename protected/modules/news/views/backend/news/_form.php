<?php

/** @var $form MyBackendActiveForm */
$form = $this->beginWidget('MyBackendActiveForm');

echo $form->beginForm();
echo $form->errorSummary($model);


$this->widget('application.zii.widgets.TabbedLangFields', array(
    'model' => $model,
    'form' => $form,
    'fields' => array('title','text')
));


echo $form->buttons($model);
echo $form->endForm();

$this->endWidget();
