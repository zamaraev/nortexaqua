<?php
foreach ($fields as $field) {

    if (isset($model->getMetaData()->columns["$field$field_postfix"])) {

        echo $form->beginField();
        $field_type = $model->getMetaData()->columns["$field$field_postfix"]->dbType;

        if ($field_type == 'text') {
            echo $form->myTextArea($model, "$field$field_postfix");
        } else {
            echo $form->renderField($model, "$field$field_postfix");
        }
        echo $form->endField();
    }
}
