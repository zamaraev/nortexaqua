<?php

/**
 * Класс модели для таблицы "News".
 *
 * @property integer $id
 * @property integer $status
 * @property double $price
 * @property double $received
 * @property string $created_at
 * @property string $updated_at
 *
 */
class Contacts extends MyActiveRecord
{


    /**
     * Правила.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('status_activ, name, email, message, created_at', 'required'),
            array('email','email','on'=>'register'),
            array('status_activ, updated_at', 'safe'),
            array('name, email, message, phone, created_at, updated_at, status_activ', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Связи.
     *
     * @return array
     */
    public function relations()
    {
        return array(

        );
    }

    /**
     * @param string $className
     * @return self
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
