<?php

class m130623_154735_contacts_add_colum extends MyDbMigration
{
    private $_table = 'Contacts';

    public function safeUp()
    {
        $this->addColumn($this->_table, 'status_activ', 'boolean NOT NULL DEFAULT false');
    }

    public function safeDown()
    {
        $this->dropColumn($this->_table, 'status_activ', 'boolean NOT NULL DEFAULT false');
    }
}
