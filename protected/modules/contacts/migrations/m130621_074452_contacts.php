<?php

class m130621_074452_contacts extends MyDbMigration
{
    private $_table = 'Contacts';

    public function safeUp()
    {
        $this->createTable($this->_table, array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'email' => 'string NOT NULL',
            'phone' => 'string',
            'message' => 'text NOT NULL',
            'created_at' => 'timestamp NOT NULL DEFAULT NOW()',
            'updated_at' => 'timestamp',
        ), 'ENGINE InnoDB');
    }

    public function safeDown()
    {
        $this->dropTable($this->_table);
    }
}
