<?php

/** @var $form MyBackendActiveForm */
$form = $this->beginWidget('MyBackendActiveForm');

echo $form->beginForm();
echo $form->errorSummary($model);

echo $form->beginField();
echo $form->renderField($model, 'name');
echo $form->endField();

echo $form->beginField();
echo $form->renderField($model, 'email');
echo $form->endField();

echo $form->beginField();
echo $form->renderField($model, 'message');
echo $form->endField();

echo $form->beginField();
echo $form->renderField($model, 'phone');
echo $form->endField();


echo $form->beginField();
echo $form->myCheckBox($model, 'status_activ');
echo $form->endField();


echo $form->buttons($model);
echo $form->endForm();

$this->endWidget();
