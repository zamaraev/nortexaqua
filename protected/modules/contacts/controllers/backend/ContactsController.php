<?php

class ContactsController extends MyBackendController
{
    public $model = 'Contacts';

    public function actions()
    {
        return array(
            'index' => 'application.components.backend.actions.RedirectToAdmin',
            'admin' => 'application.components.backend.actions.Admin',
            'update' => 'application.components.backend.actions.Update',
            'create' => 'application.components.backend.actions.Create',
            'delete' => 'application.components.backend.actions.Delete',
        );
    }

    public function adminAttributes()
    {
        return array(
            array('id'),
            array(
                'name' => 'name',
            ),
            array(
                'name' => 'status_activ',
                'value' => '($data->status_activ == 1) ? "Да" : "Нет"',
                'sortable' => false,
            ),
            array('created_at'),
            array('updated_at'),
        );
    }
}
