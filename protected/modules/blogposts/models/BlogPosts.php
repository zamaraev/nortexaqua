<?php

/**
 * Класс модели для таблицы "blogposts".
 *
 */
class BlogPosts extends MyActiveRecord
{


    /**
     * Правила.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('title_lang1, title_lang2, text_lang1, text_lang2, tags, date', 'required'),
            array('email','email','on'=>'register'),
            array('date', 'safe'),
            array('title_lang1, title_lang2, text_lang1, text_lang2, tags, date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Связи.
     *
     * @return array
     */
    public function relations()
    {
        return array(

        );
    }

    /**
     * @param string $className
     * @return self
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getAllForIndexPage($tag, $offset = 0 ) {
        return array_chunk($this->getLimitDataBlogPosts($tag, $offset, 3),1);
    }

    public function getAllTagsToStr( $offset = 0) {
        $tags_str = "";
        $BlogPosts = $this->getLimitDataBlogPosts(null,$offset);
        foreach ($BlogPosts as $tag) {
            $tags_str .= $tag['tags'].",";
        }
        return $tags_str;
    }

    public function getPopular_tags()
    {
        $text_tags = $this->getAllTagsToStr();
        $tags = explode(",", htmlspecialchars($text_tags));
        $arr = array_count_values($tags);
        arsort($arr);
        $arrsort = array();
        foreach ($arr as $key => $val) {
            $arrsort[] = array($key => $val);
        }
        $popular_tags = array_slice($arrsort, 0, 10);
        return $popular_tags;
    }

    private function getLimitDataBlogPosts($tag=null,$offset = 0, $limit =0 )
    {
        $cr = new CDbCriteria();
        if ($limit != 0)
            $cr->limit = $limit;
        $cr->order = 'date desc';
        $cr->offset = $offset;

        if (isset($tag)) {
            $cr->addSearchCondition('tags', $tag, true);
        }

        $BlogPosts=$this->findAll($cr);

        $BlogPosts_array = array();
        foreach ( $BlogPosts  as $BlogPost ) {

            $BlogPosts_array[] = array( 'id' => $BlogPost->id, 'title' => $BlogPost->title_lang1, 'text' => $BlogPost->text_lang1,'tags'=> $BlogPost->tags, 'date'=>$BlogPost->date);
        }
        return  $BlogPosts_array;
    }

}
