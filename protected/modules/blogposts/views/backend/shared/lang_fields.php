<?php
foreach ($fields as $field) {

    if (isset($model->getMetaData()->columns["$field$field_postfix"])) {

        echo $form->beginField();
        $field_type = $model->getMetaData()->columns["$field$field_postfix"]->dbType;

        if ($field_type == 'text') {
            echo $form->myTextArea($model, "$field$field_postfix");
            /*echo $this->widget( 'application.extensions.EImperaviRedactor.EImperaviRedactor', array(
                        'model' => $model,
                        'attribute' => "$field$field_postfix",
                        'options' => array(
                            'toolbar' => 'my',
                            'image_upload' => Yii::app()->createUrl( '/' . Yii::app()->getController()->getId() . '/upload' ),
                        ),
                        'htmlOptions' => array( 'style' => 'width: 100%; height: 200px;' ),
                    ), true );*/
        } else {
            echo $form->renderField($model, "$field$field_postfix");
        }
        echo $form->endField();
    }
}
