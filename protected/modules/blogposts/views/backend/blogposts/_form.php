<?php

/** @var $form MyBackendActiveForm */
$form = $this->beginWidget('MyBackendActiveForm');

echo $form->beginForm();
echo $form->errorSummary($model);

$this->widget('application.zii.widgets.TabbedLangFields', array(
    'model' => $model,
    'form' => $form,
    'fields' => array('title','text')
));

echo $form->beginField();
echo $form->renderField($model, 'tags');
echo $form->endField();

echo $form->beginField();
echo $form->renderField($model, 'date');
echo $form->endField();

echo $form->buttons($model);
echo $form->endForm();

$this->endWidget();
