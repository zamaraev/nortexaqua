<?php

class DevicesCommand extends CConsoleCommand
{
    public function actionGetMoney() {
        set_time_limit(0);
        $order = Orders::model()->getLast();

        $facade = new DevicesFacade($order);

        if ( $order && $order->status == Orders::STATUS_WAITING_FOR_PAYMENT) {
            $received = $facade->getMoney( $order->price );
            if ($received >= $order->price) {
                $order->setPaid();
            }
        }
    }

    public function actionPaid()
    {
        set_time_limit(0);
        $order = Orders::model()->getLast();

        $facade = new DevicesFacade($order);

        if ($this->_orderPaidAndNeedGivenOut($order)) {
            $facade->pay( $order->received - $order->price );
        }
    }

    private function _orderPaidAndNeedGivenOut($order)
    {
        return ($order && $order->status == Orders::STATUS_PAID && $order->received > $order->price);
    }
}
