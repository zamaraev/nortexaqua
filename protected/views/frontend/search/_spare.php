<?php /** @var $offer Offers */ ?>
<tr>
    <td class="col1"><a href="#"><?php echo $spare->articul ?></a></td>
    <td class="col2"><?php echo MyHtml::link($spare->manufacturer, array('/dicts/manufacturers/show', 'id' => $spare->manufacturer_id)) ?></td>
    <td class="col3"><?php echo $spare ?></td>
    <td class="col4">
        <div class="colich yes"><?php echo $offer->availability ?>
            <img src="<?php echo Yii::app()->baseUrl ?>/images/frontend/colich_yes.png" alt="" />
        </div>
    </td>
    <td class="col5"><?php echo $offer->delivery_period ?></td>
    <td class="col6"><?php echo Yii::t('app', '{n} рубль|{n} рубля|{n} рублей|{n} рубля', $offer->price); ?></td>
    <td class="col6"><?php echo $offer->supplier->rating; ?></td>
    <td class="col6">
        <input type="button" name="minus" value="-" data-id="<?php echo $offer->id ?>" />
        <input type="text" name="quantity" value="1" size="1" data-id="<?php echo $offer->id ?>" />
        <input type="button" name="plus" value="+" data-id="<?php echo $offer->id ?>" />
    </td>
    <td class="col7"><input type="button" class="button addtocart" value="В корзину" data-id="<?php echo $offer->id ?>" /></td>
</tr>
