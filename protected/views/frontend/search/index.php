<?php
$this->widget('ext.EColorBox.EColorBox', array(
    'target' => '.addtocart',
));
?>

<script type="text/javascript">
    $(function() {
        $('.addtocart').click(function() {
            $.colorbox({
                href: '<?php echo $this->createUrl('cart/add') ?>',
                data: {id: $(this).data('id'), quantity: $('input[name=quantity][data-id=' + $(this).data('id') + ']').val()}
            });
        });

        $('input[name=minus]').click(function() {
            var offerId = $(this).data('id');
            var q = $('input[name=quantity][data-id=' + offerId + ']');
            if (parseInt(q.val())-1 > 1) {
                q.val(parseInt(q.val())-1);
            } else {
                q.val(1);
            }
        });

        $('input[name=plus]').click(function() {
            var offerId = $(this).data('id');
            var q = $('input[name=quantity][data-id=' + offerId + ']');
            q.val(parseInt(q.val())+1);
        });
    });
</script>

<div id="search_box">
    <?php
    $spareIds = array();
    if ($exactResult != false && $exactResult['total'] > 0) {
        $spareIds = array($exactResult['matches'][0]['id']);
    } else if ($similarResults != false && $similarResults['total'] > 0) {
        foreach ($similarResults['matches'] as $match) {
            $spareIds[] = $match['id'];
        }
    }

    if (empty($spareIds)) {
        ?>
        <p>Ничего не найдено</p>
        <?php
    } else {
        foreach($spareIds as $spareId) {
            $spare = Spares::model()->findByPk($spareId);
            echo '<h1>' . $spare . '</h1>';
            if (!empty($spare->offers)) {
                ?>
                <table border="0" width="100%" class="table_search" cellpadding="0" cellspacing="0">
                    <tr>
                        <th class="col1">Номер детали</th>
                        <th class="col2">Фирма</th>
                        <th class="col3">Название</th>
                        <th class="col4">Наличие</th>
                        <th class="col5">Срок поставки</th>
                        <th class="col6">Цена партнера</th>
                        <th class="col6">Рейтинг партнера</th>
                        <th class="col6">Количество</th>
                        <th class="col7"></th>
                    </tr>
                    <?php
                    foreach ($spare->offers as $offer) {
                        $this->renderPartial('_spare', array('spare' => $spare, 'offer' => $offer));
                    }
                    ?>
                </table>

                <?php
                if (!empty($spare->analogs)) {
                    ?>
                    <h2>Замена аналогом</h2>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <?php
                        foreach ($spare->analogs as $analog) {
                            foreach ($analog->offers as $offer) {
                                $this->renderPartial('_spare', array('spare' => $analog, 'offer' => $offer));
                            }
                        }
                        ?>
                    </table>
                    <?php
                }
            } else {
                ?>
                <p>Предложений нет</p>
                <?php
            }
        }
    }
    ?>
</div>
