<section id="content">
    <div class="ic">More Website Templates @ TemplateMonster.com - February 13, 2012!</div>
    <div class="container_12">
        <div class="wrapper">
            <article class="grid_4" style="width: 620px">
                <div class="indent-right">
                    <div class="maxheight img-indent-bot">
                        <h3><?php echo Yii::t('site', 'welcome'); ?></h3>
                        <?php echo Yii::t('site', 'index'); ?>
                    </div>

                </div>
            </article>

            <article class="grid_4">
                <div class="indent-left">
                    <div class="maxheight img-indent-bot">
                        <h3><?php echo Yii::t('site', 'recent_news'); ?></h3>

                        <div class="scroll" style="height:400px;">
                            <?php echo $this->renderPartial('_news', array('news' => $news)) ?>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</section>