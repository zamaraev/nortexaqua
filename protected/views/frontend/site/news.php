<section id="content">
    <div class="ic">More Website Templates @ TemplateMonster.com - February 13, 2012!</div>
    <div class="container_12">
        <div class="wrapper">
            <article class="grid_12">
                <h3> <?= Yii::app()->language == 'ru' ? $new['title_lang1'] : $new['title_lang2']?>  </h3>
                <div class="wrapper">
                    <p class="indent-bot"><span
                            class="color-5">Опубликовано:<?= $new['created_at'] ?></p>
                    <?= Yii::app()->language == 'ru' ? str_replace("<hr />", "", $new['text_lang1']) : str_replace("<hr />", "", $new['text_lang2']); ?>
                </div>
            </article>
        </div>
    </div>
</section>
