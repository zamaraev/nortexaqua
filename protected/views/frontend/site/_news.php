<?php
Yii::app()->language == 'ru' ? $lang = 1 : $lang = 2;
foreach ($news as $new) {
    if (($lang == 1 && trim ($new['title_lang1']) != '') || ($lang == 2 && trim ($new['title_lang2']) != '')) {
        ?>
        <time class="tdate-1" datetime="<?= $new['created_at'] ?>"><a class="link"
                                                                      href="<?= $this->createUrl("news", array('id' => $new['id'])); ?>">Опубликовано: <?= $new['created_at'] ?></a>
        </time>
    <?php } ?>
    <h1><?= Yii::app()->language == 'ru' ? $new['title_lang1'] : $new['title_lang2'] ?></h1>
    <br>
    <?php if (Yii::app()->language == 'ru') { ?>
        <p><?= substr($new['text_lang1'], 0, strrpos($new['text_lang1'], "<hr />")); ?></p>
    <?php } else { ?>
        <p><?= substr($new['text_lang2'], 0, strrpos($new['text_lang2'], "<hr />")); ?></p>
    <?php
    }
} ?>