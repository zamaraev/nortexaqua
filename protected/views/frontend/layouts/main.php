﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8" />
    <title>Nortexaqua</title>
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/frontend/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/frontend/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/frontend/grid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/frontend/gallery.css" type="text/css" media="screen">

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/js/frontend/cufon-yui.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/js/frontend/cufon-replace.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/js/frontend/Asap_400.font.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/js/frontend/Asap_italic_400.font.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/js/frontend/FF-cash.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/js/frontend/jquery.equalheights.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/js/frontend/jquery.cycle.all.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/libs/callme/js/callme.js"></script>

    <script src="<?php echo Yii::app()->baseUrl ?>/js/frontend/todo.js" type="text/javascript"></script>

    <script>
        $('#banners')
            .cycle({
                fx: 'fade',
                delay: 7000 ,
                timeout: 30000,
                manualTrump:false,
                cleartypeNoBg: true,
                next: '#next',
                prev: '#prev'
            });
    </script>
    <script type="text/javascript">
        todo.onload(function(){
            todo.gallery('gallery');
        });
    </script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/frontend/js/html5.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/frontend/css/ie.css" type="text/css" media="screen">
    <![endif]-->
</head>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-41983797-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
<body id="page5">

<div id="callme">
    <input type="button" id="viewform" class="callme_viewform" style="opacity: 1;">
</div>

<?php echo $this->renderPartial('/layouts/_header') ?>

<?php echo $content; ?>

<?php echo $this->renderPartial('/layouts/_footer') ?>
<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>
