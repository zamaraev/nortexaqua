<header>
    <div class="container_12">
        <div id="language-selector" style="float:right; margin:5px;">
            <?php
            $this->widget('application.web.widgets.LanguageSelector');
            ?>
        </div>
        <div class="wrapper">
            <div class="grid_12">
                <div class="wrapper border-bot">
                    <h1>
                        <a href="<?= $this->createUrl("/site"); ?>"><img
                                src="<?php echo Yii::app()->baseUrl ?>/images/frontend/log.png" height="60" width="65"
                                alt=""/><?php echo Yii::t('header', 'logo'); ?></a></h1>
                    <nav>
                        <ul class="menu">
                            <li><a class="<?= $this->id == 'site' ? 'active' : '' ?>"
                                   href="<?= $this->createUrl("/site"); ?>"><?php echo Yii::t('header', 'site'); ?></a>
                            </li>
                            <li><a class="<?= $this->id == 'product' ? 'active' : '' ?>"
                                   href="<?= $this->createUrl("/product"); ?>"><?php echo Yii::t('header', 'products'); ?></a>
                            </li>
                            <li><a class="<?= $this->id == 'blogs' ? 'active' : '' ?>"
                                   href="<?= $this->createUrl("/blogs"); ?>"><?php echo Yii::t('header', 'blog'); ?></a>
                            </li>
                            <li><a class="<?= $this->id == 'contact' ? 'active' : '' ?>"
                                   href="<?= $this->createUrl("/contact"); ?>"><?php echo Yii::t('header', 'contacts'); ?></a>
                            </li>
                            <li><a class="<?= $this->id == 'reviews' ? 'active' : '' ?>"
                                   href="<?= $this->createUrl("/reviews"); ?>"><?php echo Yii::t('header', 'reviews'); ?></a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="wrapper">
                    <div id="banners" class="border-bot" style="height: 250px">
                        <div>
                            <h1>
                                <img src="<?php echo Yii::app()->baseUrl ?>/images/frontend/1_1.png"alt=""/>
                            </h1>
                            <strong class="title-1"><?php echo Yii::t('header', 'title_1'); ?></strong>
                            <strong class="title-2"><?php echo Yii::t('header', 'title_2'); ?></strong>


                        </div>
                        <div>
                            <h1>
                                <img src="<?php echo Yii::app()->baseUrl ?>/images/frontend/1_1.png"alt=""/>
                            </h1>
                            <strong class="title-1"><?php echo Yii::t('header', 'title_1'); ?></strong>
                            <strong class="title-2"><?php echo Yii::t('header', 'title_2'); ?></strong>
                        </div>
                        <div>
                            <h1>
                                <img src="<?php echo Yii::app()->baseUrl ?>/images/frontend/1_1.png"alt=""/>
                            </h1>
                            <strong class="title-1"><?php echo Yii::t('header', 'title_1'); ?></strong>
                            <strong class="title-2"><?php echo Yii::t('header', 'title_2'); ?></strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
