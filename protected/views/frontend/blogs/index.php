<script type="text/javascript">
    var APP = {
        baseUrl: '<?php echo Yii::app()->getRequest()->getBaseUrl(true) ?>',
        baseRequestUrl: '<?php echo Yii::app()->getRequest()->getScriptUrl() ?>?r='
    };

    APP._tag = '<?= isset( $_GET['tag'] ) ? $_GET['tag'] : "" ?>';

    $(function () {
        blog_offset = 3;
        $.ajax({
            type: "GET",
            url: APP.baseUrl + "/blogs",
            data: {offset: blog_offset, tag: APP._tag},
            success: function (html) {
                if (html.trim() == "") {
                    $(".more_blog").hide();
                }
            }
        });
        //Загрузка дополнительных блогов
        $("#blog_box .more_blog a").click(function () {
            $.ajax({
                type: "GET",
                url: APP.baseUrl + "/blogs",
                data: {offset: blog_offset, tag: APP._tag},
                success: function (html) {
                    $("#blog_box #center").append(html);
                    blog_offset = blog_offset + 3;

                    $.ajax({
                        type: "GET",
                        url: APP.baseUrl + "/blogs",
                        data: {offset: blog_offset, tag: APP._tag},
                        success: function (html) {
                            if (html.trim() == "") {
                                $(".more_blog").hide();
                            }
                        }
                    });
                }
            });
            return false;
        });
    });
</script>

<section id="content">
    <div class="main" id="blog_box">
        <div class="container_12">
            <div class="wrapper">
                <article class="grid_8">
                    <div id="center">
                        <?php echo $this->renderPartial('_list', array('BlogPosts' => $BlogPosts)); ?>
                    </div>
                    <div class="buttons more_blog">
                        <a class="button" href="#""><span><?php echo Yii::t('blogs', 'more'); ?></span></a>
                    </div>

                </article>

                <article class="grid_4">

                    <div class="indent-left">
                        <h4>Теги</h4>

                        <p class="p1">Популярные теги:</p>

                        <ul class="list-1">
                            <?php foreach ($popular_tags as $key => $popular_tag) { ?>
                                <?php foreach ($popular_tag as $key => $val) { ?>
                                    <? if (!empty($key)) { ?>
                                        <li>
                                            <a href="<?= $this->createUrl("/blogs/index", array('tag' => $key)) ?>"><?php echo $key ?> </a>
                                        </li>
                                    <?php
                                    }
                                }
                            } ?>
                        </ul>

                    </div>

                </article>

            </div>

        </div>


    </div>
    <div class="block"></div>

</section>

