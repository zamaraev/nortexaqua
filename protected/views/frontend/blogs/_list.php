<? foreach ($BlogPosts as $BlogPost) { ?>
    <?php foreach ($BlogPost as $key) { ?>

        <div class="indent-left">
            <h4><?= $key['title'] ?></h4>

            <div class="wrapper prev-indent-bot2">

                <div class="extra-wrap">
                    <?php echo strip_tags(substr($key['text'], 0, strrpos($key['text'], "<hr />"))); ?>
                </div>
                <div class="info">
                    <div class="fleft">
                        <a href="<?= $this->createUrl("/blogs/show", array('id' => $key['id'])) ?>"
                           class="more"><?php echo Yii::t('blogs', 'readmore'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    <? }
} ?>