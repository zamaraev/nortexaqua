<section id="content">
    <div class="ic">More Website Templates @ TemplateMonster.com - February 13, 2012!</div>
    <div class="container_12">
        <div class="wrapper">
            <article class="grid_12">
                <h3> <?= $product['title'] ?>  </h3>
                <div class="wrapper">
                    <figure class="img-indent">
                    <a href="/<?php echo $product->uploadTo('picture1') ?>" rel="gallery[2]"
                       title="<?php echo $product['title'] ?>">
                        <img src="/<?php echo $product->uploadTo('picture') ?>" width="220"
                             height="164" alt=""></a></figure>
                    <?= str_replace("<hr />", "", $product['text']); ?>
                </div>
            </article>
        </div>
    </div>
</section>
