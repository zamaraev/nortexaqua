<?php $i = 1;
foreach ($products as $item) {
    if ($i % 1 == 0) { ?>
        <div class="grid_6 alpha">
            <div class="indent-right">
                <div class="wrapper">
                    <figure class="img-indent">
                        <a href="/<?php echo $item->uploadTo('picture1') ?>" rel="gallery[2]"
                           title="<?php echo $item['title'] ?>">
                            <img src="/<?php echo $item->uploadTo('picture') ?>" width="220"
                                 height="164" alt=""></a>
                    </figure>
                    <div class="extra-wrap">
                        <h5 class="prev-indent-bot"><?php echo $item['title'] ?></h5>

                        <p class="indent-bot"><?php echo strip_tags(substr($item['text'], 0, strrpos($item['text'], "<hr />"))); ?></p>
                        <a class="button" href="<?= $this->createUrl("show", array('id'=>$item['id'])); ?>"><?php echo Yii::t('products', 'more'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    <? } else { ?>
        <div class="grid_6 omega">
            <div class="indent-left3">
                <div class="wrapper">
                    <figure class="img-indent"><img src="<?php echo $item['picture'] ?>" alt=""></figure>
                    <div class="extra-wrap">
                        <h5 class="prev-indent-bot"><?php echo $item['title'] ?></h5>

                        <p class="indent-bot"><?php echo strip_tags(substr($item['text'], 0, strrpos($item['text'], "<hr />"))); ?></p>
                        <a class="button" href="<?= $this->createUrl("show", array('id'=>$item['id'])); ?>"><?php echo Yii::t('products', 'more'); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <? $i++;
    }
} ?>