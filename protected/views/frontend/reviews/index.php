<section id="content">
    <div class="ic">More Website Templates @ TemplateMonster.com - February 13, 2012!</div>
    <div class="container_12">
        <div class="wrapper">
            <div class="maxheight img-indent-bot">
                <h3><?php echo Yii::t('reviews', 'review'); ?></h3>

                <?php $i=0; foreach ($reviews as $review) {
                    if ($review['status_activ'] == 1) { ?>

                        <div class="wrapper prev-indent-bot">
                            <figure class="img-indent"><img
                                    src="<?php echo Yii::app()->baseUrl ?>/images/frontend/dialog.png" alt=""></figure>
                            <div class="extra-wrap">
                                <h6><?php echo $review['name'] ?></h6>
                                <?php echo $review['message'] ?>
                            </div>
                        </div>
                    <?php $i++; }
                } ?>
            </div>
            <?php if ($i==0) { ?>
            <div class="maxheight img-indent-bot">
                <p>На данный момент отзывов нет</p>
            </div>
            <?php } ?>
            <div class="buttons">
                <a class="button" style=""
                   href="<?= $this->createUrl("/contact"); ?>"><?php echo Yii::t('reviews', 'new_review'); ?></a>
            </div>
        </div>
    </div>
</section>
