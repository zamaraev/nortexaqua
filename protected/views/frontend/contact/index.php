<section id="content"><div class="ic"></div>
    <div class="container_12">
        <div class="wrapper">
            <article class="grid_8" style="width: 500px">
                <div class="indent-right">
                    <h3 class="prev-indent-bot"><?php echo Yii::t('contact', 'contact_form'); ?></h3>
                    <?php echo $this->renderPartial('_form') ?>
                </div>
            </article>
            <article class="grid_4" style="width: 420px">
                <div class="indent-left3">
                    <h3 ><?php echo Yii::t('contact', 'recent_news'); ?></h3>
                    <dl>
                        <dt><?php echo Yii::t('contact', 'text'); ?></dt>
                        <dd><span style="width: 300px"><?php echo Yii::t('contact', 'telephone'); ?>:</span>8-800-333-44-11</dd>
                        <dd><span>E-mail:</span><a href="#">info@nortexaqua.ru</a></dd>
                        <dd>г.Тамбов</dd>
                    </dl>

                </div>
            </article>
        </div>
    </div>
</section>
