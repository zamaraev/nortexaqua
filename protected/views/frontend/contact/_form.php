<div class="alert alert-error" style="display: none; width: 400px;">
    <div class="error-description" style="color: #b94a48;"></div>
</div>
<br/>
<form id="contact-form" method="post" action="<?php echo $this->createUrl('create') ?>">
    <fieldset>
        <div class="control-group">
            <label class="control-label" for="name">Имя:</label>

            <div class="controls">
                <input type="text" id="name" name="name" class="keyboard firstname" placeholder="Введите свое имя">
            </div>
        </div>
        <br>

        <div class="control-group">
            <label class="control-label" for="email">Email:</label>

            <div class="controls">
                <input type="text" id="email" name="email" class="keyboard firstname" placeholder="Введите Email">
            </div>
        </div>
        <br>

        <div class="control-group">
            <label class="control-label" for="phone">Телефон:</label>

            <div class="controls">
                <input type="text" id="phone" name="phone" class="keyboard firstname" placeholder="Введите телефон">
            </div>
        </div>

        <div class="wrapper">
            <div class="extra-wrap">
                <div class="control-group">
                    <label class="control-label" for="text">Сообщение:</label>

                    <div class="controls">
                        <textarea rows="8" style="width: 52%" id="text" name="text" class="keyboard firstname"
                                  placeholder="Введите текст сообщения"></textarea>
                    </div>
                </div>

                <div class="clear"></div>
                <div class="buttons">

                    <a class="button" href="#" onClick="document.getElementById('contact-form').reset()"><?php echo Yii::t('contact', 'clear'); ?></a>
                    <a class="button submit-request" id="button" href="#"
                       onClick="document.getElementById('contact-form').submit()"><?php echo Yii::t('contact', 'send'); ?></a>
                </div>
            </div>
        </div>
    </fieldset>
</form>

<script type="text/javascript">
    $(function () {
        document.getElementById("button").onclick = (function () {
            var r = /^\w+@\w+\.\w{2,4}$/i;
            $('.submit-request').attr('disabled', 'disabled');
            $('.error-description').html('');
            $('.control-group').removeClass('error');
            $('.alert').hide();
            name_error = false;
            mail_error = false;

            if ($('#name').val().trim() == '') {
                $('#name').closest('.control-group').addClass('error');
                name_error = true;
            }
            if ($('#text').val().trim() == '') {
                $('#text').closest('.control-group').addClass('error');
                name_error = true;
            }
            if ($('#email').val().trim() == '') {
                $('#email').closest('.control-group').addClass('error');
                name_error = true;
            }
            if (!r.test($('#email').val().trim())) {
                $('#email').closest('.control-group').addClass('error');
                mail_error = true;
            }
            if (name_error == true) {
                $('.error-description').append('Пожалуйста заполните обезательные поля <br>');
            }
            if (mail_error == true) {
                $('.error-description').append('Email указан неверно');
            }
            if (name_error == true || mail_error == true) {
                $('.alert').show();
                $('.submit-request').removeAttr('disabled');
                return false;
            } else {
                document.getElementById("contact-form").submit()
            }
        });
    });
</script>