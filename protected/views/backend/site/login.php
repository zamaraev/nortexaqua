<?php

/** @var $form MyBackendActiveForm */
$form = $this->beginWidget('MyBackendActiveForm', array(
    'focus' => '#' . get_class($model) . '_username',
));

echo $form->beginForm();

echo $form->beginField();
echo $form->renderField($model, 'username');
echo $form->endField();

echo $form->beginField();
echo $form->renderField($model, 'password');
echo $form->endField();

echo $form->beginField();
echo $form->button($model, 'Войти', true);
echo $form->endField();

echo $form->endForm();

$this->endWidget();
