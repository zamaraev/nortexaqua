<?php $this->beginContent('//layouts/layout'); ?>
    <?php if ($this->hasFlash('default')) { ?>
        <div class="alert alert-success">
            <?php echo $this->getFlash('default'); ?>
        </div>
    <?php } ?>

    <?php if ($this->hasFlash('error')) { ?>
        <div class="alert alert-error">
            <?php echo $this->getFlash('error'); ?>
        </div>
    <?php } ?>

    <?php echo $content; ?>
<?php $this->endContent(); ?>