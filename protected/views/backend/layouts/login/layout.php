<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language ?>">
<head>
    <meta charset="utf-8">

    <title><?php echo MyHtml::encode(Yii::app()->name); ?></title>

    <?php
    $myClientScript = Yii::app()->getClientScript();
    $myClientScript->registerLessFile(Yii::app()->getRequest()->baseUrl . '/css/backend/bootstrap.less');
    $myClientScript->registerCoreScript('less');
    $myClientScript->registerCoreScript('jquery');
    ?>

    <style type="text/css">
        body {
            margin-top: 200px;
        }
        .title {
            font-size: 20px;
            background-color: #f5f5f5;
            padding: 10px;
            margin-bottom: 28px;
        }
        .form-horizontal .form-actions {
            padding-left: 18px;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="span12">
        <div class="title">Авторизация</div>
        <?php echo $content; ?>
    </div>
</div>
</body>
</html>
