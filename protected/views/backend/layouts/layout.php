<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language ?>">
<head>
    <meta charset="utf-8">

    <title><?php echo MyHtml::encode(Yii::app()->name); ?></title>

    <?php
    $myClientScript = Yii::app()->getClientScript();
    $myClientScript->registerLessFile(Yii::app()->getRequest()->baseUrl . '/css/backend/bootstrap.less');
    $myClientScript->registerCoreScript('less');
    ?>

    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?php echo Yii::app()->getRequest()->baseUrl ?>/js/html5.js"></script>
    <![endif]-->

    <style type="text/css">
        .navbar-static-top {
            margin-bottom: 20px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }
    </style>
</head>

<body>

<div class="navbar navbar-static-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="brand" href="<?php echo $this->createUrl('/') ?>"><?php echo Yii::app()->name ?></a>

            <div class="nav-collapse">
                <ul class="nav">
<!--                    <li class="active"><a href="#">Home</a></li>-->
<!--                    <li><a href="#about">About</a></li>-->
<!--                    <li><a href="#contact">Contact</a></li>-->
                </ul>

                <ul class="nav pull-right">
                    <li class="divider-vertical"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php echo Yii::app()->getUser()->id ?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
<!--                            <li><a href="#">some link</a></li>-->
<!--                            <li class="divider"></li>-->
                            <li><?php echo MyHtml::link('Выход', array('/site/logout')) ?></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <div class="well sidebar-nav">
                <?php
                $this->widget(
                    'application.zii.widgets.MyMenu',
                    array(
                        'items' => array(
                            //array('label' => 'Настройки', 'itemOptions' => array('class' => 'nav-header')),
                            //array('label' => '<i class="icon-list"></i> Настройка URL', 'url' => array('/settings/FrontendRoutes'), 'visible' => Yii::app()->getUser()->checkAccess('admin')),

                            array('label' => 'Управление', 'itemOptions' => array('class' => 'nav-header')),
                            array('label' => '<i class="icon-list"></i> Новости', 'url' => array('/news/news/admin'), 'visible' => Yii::app()->getUser()->checkAccess('admin')),
                            array('label' => '<i class="icon-list"></i> Продукция', 'url' => array('/products/products/admin'), 'visible' => Yii::app()->getUser()->checkAccess('admin')),
                            array('label' => '<i class="icon-list"></i> Обратная связь', 'url' => array('/contacts/contacts/admin'), 'visible' => Yii::app()->getUser()->checkAccess('admin')),
                            array('label' => '<i class="icon-list"></i> Блог', 'url' => array('/blogposts/blogposts/admin'), 'visible' => Yii::app()->getUser()->checkAccess('admin')),
                        ),
                        'encodeLabel' => false,
                        'htmlOptions' => array('class' => 'nav nav-list'),
                    )
                );
                ?>
            </div>
        </div>

        <div class="span8">
            <?php
            if ($this->tabs !== null) {
                $this->widget(
                    'application.zii.widgets.MyMenu',
                    array(
                        'items' => $this->tabs,
                        'encodeLabel' => false,
                        'htmlOptions' => array('class' => 'nav nav-pills'),
                    )
                );
            }

            if (!empty($this->breadcrumbs)) {
                $this->widget('application.zii.widgets.MyBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                    'separator' => '<span class="divider">/</span>',
                    'htmlOptions' => array('class' => 'breadcrumb'),
                ));
            }
            ?>

            <?php echo $content; ?>
        </div>

        <?php if ($this->menu !== null) { ?>
            <div class="span2">
                <div class="well sidebar-nav">
                    <?php
                    $this->widget(
                        'application.zii.widgets.MyMenu',
                        array(
                            'items' => $this->menu,
                            'encodeLabel' => false,
                            'htmlOptions' => array('class' => 'nav nav-list'),
                        )
                    );
                    ?>
                </div>
            </div>
        <?php } ?>
    </div>

    <hr>
</div>

<script src="<?php echo Yii::app()->getRequest()->baseUrl ?>/js/bootstrap/bootstrap-dropdown.js"></script>

</body>
</html>
