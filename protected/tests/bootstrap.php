<?php

// change the following paths if necessary
$config=dirname(__FILE__).'/../config/test.php';

// disable Yii error handling logic
defined('YII_ENABLE_EXCEPTION_HANDLER') or define('YII_ENABLE_EXCEPTION_HANDLER',false);
defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER',false);

require_once dirname(__FILE__) . '/../../libs/vendor/yii/YiiBase.php';

class Yii extends YiiBase {
    /**
     * Для автокомплита IDE.
     *
     * @static
     * @return MyWebApplication
     */
    public static function app()
    {
        return parent::app();
    }
}

Yii::import('system.test.CTestCase');
Yii::import('system.test.CDbTestCase');
Yii::import('system.test.CWebTestCase');
//require_once(dirname(__FILE__).'/WebTestCase.php');

Yii::createWebApplication($config);
