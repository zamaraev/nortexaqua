<?php

class ContactController extends MyFrontendController
{
    protected function beforeAction( $action ) {
        Yii::import( 'application.modules.contacts.models.*' );
        Yii::import( 'application.commands.*' );

        return parent::beforeAction( $action );
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionCreate()
    {
        if ( Yii::app()->request->isPostRequest ) {
         $contacts = new Contacts();
            $contacts->name = $_POST['name'];
            $contacts->email = $_POST['email'];
            $contacts->phone = $_POST['phone'];
            $contacts->message = $_POST['text'];
            if ($contacts->save()) {
                MyMailer::getInstance()
                    ->setText('Отправитель: '.$contacts->name.' ,текст сообщения: '. $contacts->message.', контактные данные: '. $contacts->phone.',mail: '.$contacts->email)
                    ->setSubject( 'ответ' )
                    ->setTo( 'info@nortexaqua.ru' )
              //      ->setView( 'template/message' )
                    ->setContentType( 'text/html' )
                    ->send();
                $this->render('index');
            }
        }
    }

    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
}
