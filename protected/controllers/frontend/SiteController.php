<?php

class SiteController extends MyFrontendController
{
    public $menu_groups;
    public $item_of_day;

    protected function beforeAction($action)
    {
        Yii::import('application.modules.carts.models.*');
        Yii::import('application.modules.orders.models.*');
        Yii::import('application.modules.settings.models.*');
        Yii::import('application.modules.menu.models.*');
        Yii::import('application.extensions.TerminalDevices.devices.Printer.*');
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $news = News::model()->findAll(array('order'=>'created_at DESC'));
        $this->render('index', array('news'=>$news));
    }

    public function actionNews($id)
    {
        $new = News::model()->findByPk($id);
        $this->render('news', array('new'=>$new));
    }




    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
}
