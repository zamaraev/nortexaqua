<?php

class BlogsController extends MyFrontendController
{

    public function actionIndex()
    {
        $offset = 0;
        $tag = null;
        if (isset($_GET['offset']) && !empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        }
        if (isset($_GET['tag']) && !empty($_GET['tag'])) {
            $tag = $_GET['tag'];
        }
        $BlogPosts = BlogPosts::model()->getAllForIndexPage($tag,$offset);
        if (Yii::app()->request->isAjaxRequest) {

            $this->renderPartial('_list', array('BlogPosts' => $BlogPosts));
        } else {

            $this->render('index', array('BlogPosts' => $BlogPosts, 'popular_tags' => BlogPosts::model()->getPopular_tags()));
        }

    }

    public function actionShow($id)
    {
        $BlogPost = BlogPosts::model()->findByPk($id);
        $this->render('show', array('BlogPost'=>$BlogPost));
    }


    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
}
