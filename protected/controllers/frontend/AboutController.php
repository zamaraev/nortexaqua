<?php

class AboutController extends MyFrontendController
{


    public function actionIndex()
    {
        $this->render('index');
    }

    private function _sendMessage($message)
    {
        echo json_encode(array(
            'type' => 'message',
            'data' => $message,
            'id' => 1,
            'reply' => false)) . PHP_EOL;

        //PUSH THE data out by all FORCE POSSIBLE
        ob_flush();
        flush();
    }


    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
}
