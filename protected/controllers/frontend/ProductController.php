<?php

class ProductController extends MyFrontendController
{
    protected function beforeAction($action)
    {

        Yii::import('application.modules.products.models.*');
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $products = Products::model()->findAll();
        $this->render('index', array('products'=>$products));
    }

    public function actionShow($id)
    {
        $product = Products::model()->findByPk($id);
        $this->render('show', array('product'=>$product));
    }


    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
}
