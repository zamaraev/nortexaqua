<?php

class ReviewsController extends MyFrontendController
{

    public function actionIndex()
    {
        $contacts = Contacts::model()->findAll();
        $this->render('index',array('reviews'=>$contacts));
    }



    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
}
