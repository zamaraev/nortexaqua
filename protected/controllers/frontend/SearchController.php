<?php

class SearchController extends MyFrontendController
{
    public function actionIndex()
    {
        $query = $this->getPost('query');
        $exactResult = false;
        $similarResults = false;
        if ($query !== null) {
            $cl = new MySphinxClient('asiashop');
            $exactResult = $cl->query(str_replace('-', '', preg_replace('/\s/', '', $query)), false);
            if ($exactResult['total'] == 0) {
                $similarResults = $cl->query(str_replace('-', '', preg_replace('/\s/', '', $query)));
            }
        }

        $this->render('index', array(
            'exactResult' => $exactResult,
            'similarResults' => $similarResults,
        ));
    }
}
