<?php

class SiteController extends MyBackendController
{
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }

    public function actionLogin()
    {
        $this->layout = '//layouts/login/main';
        $model = new BackendLoginForm;

        $post = Yii::app()->getRequest()->getParam(get_class($model));
        if ($post !== null) {
            $model->attributes = $post;
            if ($model->validate() && $model->login()) {
                $this->redirect(array('/' . Yii::app()->defaultController));
            }
        }

        $this->render('login', array('model' => $model));
    }

    public function actionLogout()
    {
        Yii::app()->getUser()->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}
