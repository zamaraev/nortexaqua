<?php

class SaleController extends My1cController
{
    private $_dir;

    public function init()
    {
        Yii::import('application.modules.clients.models.*');
        $this->_dir = dirname(__FILE__) . '/../../../uploads/1c/';
        parent::init();
    }

    public function actionCheckAuth()
    {
        $cookieName = MyString::random(16);
        $cookieValue = MyString::random(32);

        Yii::app()->getCache()->set('1cCookieName', $cookieName, 600);
        Yii::app()->getCache()->set('1cCookieValue', $cookieValue, 600);

        printf("%s\n%s\n%s", 'success', $cookieName, $cookieValue);
    }

    public function actionInit()
    {
        $limit = ini_get('post_max_size');

        switch (strtolower(substr($limit, -1))) {
            case 'm':
                $limit = (int)$limit * 1048576;
                break;

            case 'k':
                $limit = (int)$limit * 1024;
                break;

            case 'g':
                $limit = (int)$limit * 1073741824;
                break;

            default:
                $limit = (int)$limit;
                break;
        }

        printf("zip=no\nfile_limit=%s", $limit);
    }

    public function actionQuery()
    {
        $orders = Orders::model()->for1c()->findAll();
        $transactions = Transactions::model()->for1c()->findAll();
        $clients = Clients::model()->for1c()->findAll();

        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>
            <КоммерческаяИнформация ДатаФормирования="' . $this->date() . '" ВерсияСхемы="2.04"/>
        ');

        $owner = $xml->addChild('Владелец');
        $owner->addChild('UUID', '59f239a6-0383-4689-a625-419b486d1746');
        $owner->addChild('Наименование', 'Интернет-магазин');

        $xmlOrders = $xml->addChild('Контрагенты');
        foreach($clients as $client) {
            /** @var $client Clients */
            $xmlOrder = $xmlOrders->addChild('Клиент');
            $xmlOrder->addChild('UUID', $client->external_id);
            $xmlOrder->addChild('ИдСайта', $this->generateId($client, $client->created_at, $client->id));
            $xmlOrder->addChild('Наименование', $client->stringFor1c());
            $xmlOrder->addChild('Удален', 'false');
            $xmlOrder->addChild('ТипКлиента', $client->typeFor1c());
            $xmlOrder->addChild('РазмерСкидки', $client->discount);

            if ($client->informationSource) {
                $xmlOrderSource = $xmlOrder->addChild('ИсточникИнформации');
                $xmlOrderSource->addChild('UUID', $client->informationSource->external_id);
                $xmlOrderSource->addChild('Наименование', $client->informationSource->title);
            }

            $xmlOrderClient = $xmlOrder->addChild('КонтактноеЛицо');
            $xmlOrderClient->addChild('Фамилия', $client->last_name);
            $xmlOrderClient->addChild('Имя', $client->first_name);
            $xmlOrderClient->addChild('Отчество', $client->patronymic_name);

            $xmlOrderPhone = $xmlOrder->addChild('КонтактныйТелефон');
            $xmlOrderPhone->addChild('КодСтраны', trim($client->phone['country_code'], '+'));
            $xmlOrderPhone->addChild('КодГорода', $client->phone['city_code']);
            $xmlOrderPhone->addChild('Номер', $client->phone['phone']);
            $xmlOrderPhone->addChild('ВнутреннийНомер', $client->phone['internal']);

            $xmlOrder->addChild('ЭлектроннаяПочта', $client->email);

            $xmlOrderAdress = $xmlOrder->addChild('ПочтовыйАдрес');
            $xmlOrderAdress->addChild('Индекс', $client->post_address_zip);
            $xmlOrderAdress->addChild('Город', $client->post_address_city);
            $xmlOrderAdress->addChild('Улица', $client->post_address_street);
            $xmlOrderAdress->addChild('Дом', $client->post_address_house);
            $xmlOrderAdress->addChild('Корпус', $client->post_address_housing);
            $xmlOrderAdress->addChild('КвартираОфис', $client->post_address_room);

            $xmlOrder->addChild('ДополнительнаяИнформация', $client->comments);

            if (in_array($client->type, array(Clients::TYPE_COMPANY, Clients::TYPE_PRIVATE_ENTREPRENEUR))) {
                $xmlOrderLaw = $xmlOrder->addChild('ХарактеристикиЮрЛица');
                $xmlOrderLaw->addChild('Наименование', $client->organization);
                $xmlOrderLaw->addChild('ИНН', $client->inn);
                $xmlOrderLaw->addChild('КПП', $client->kpp);
                $xmlOrderLaw->addChild('ОГРН', $client->ogrnip);
                $xmlOrderLawBank = $xmlOrderLaw->addChild('БанковскиеРеквизиты');
                $xmlOrderLawBank->addChild('РасчетныйСчет', $client->bank_rs);
                $xmlOrderLawBank->addChild('БИК', $client->bank_bik);
                $xmlOrderLawBank->addChild('КоррСчет', $client->bank_correspondent_account);
                $xmlOrderLawAdress = $xmlOrderLaw->addChild('ЮридическийАдрес');
                $xmlOrderLawAdress->addChild('Индекс', $client->legal_address_zip);
                $xmlOrderLawAdress->addChild('Город', $client->legal_address_city);
                $xmlOrderLawAdress->addChild('Улица', $client->legal_address_street);
                $xmlOrderLawAdress->addChild('Дом', $client->legal_address_house);
                $xmlOrderLawAdress->addChild('Корпус', $client->legal_address_housing);
                $xmlOrderLawAdress->addChild('КвартираОфис', $client->legal_address_room);
            }
        }

        $xmlOrders = $xml->addChild('Заказы');
        foreach ($orders as $order) {
            /** @var $order Orders */
            $xmlOrder = $xmlOrders->addChild('ЗаказПокупателя');
            $xmlOrder->addChild('UUID', $order->external_id);
            $xmlOrder->addChild('ИдСайта', $this->generateId($order, $order->created_at, $order->id));
            if ($order->transactions) {
                $xmlOrder->addChild('НомерДокумента', $order->transactions[0]->id);
            }
            $xmlOrder->addChild('ДатаДокумента', $this->date(strtotime($order->created_at)));
            $xmlOrder->addChild('Удален', 'false');
            $xmlOrder->addChild('СуммаДокумента', sprintf('%.2f', $order->price));

            $xmlOrderClient = $xmlOrder->addChild('Клиент');
            $xmlOrderClient->addChild('UUID', $order->client->external_id);
            $xmlOrderClient->addChild('ИдСайта', $this->generateId($order->client, $order->client->created_at, $order->client->id));
            $xmlOrderClient->addChild('Наименование', $order->client->stringFor1c());

            $xmlOrderStatus = $xmlOrder->addChild('СтатусЗаказа');

            $statuses = $order->statusesFor1c();
            $xmlOrderStatus->addChild('ЗначениеСтатуса', $statuses[$order->status]);
            $xmlOrderStatus->addChild('ДатаИзменения', $this->date(strtotime($order->updated_at)));

            $xmlOrderParts = $xmlOrder->addChild('Товары');
            foreach ($order->orderOffers as $orderOffer) {
                $xmlOrderPart = $xmlOrderParts->addChild('Товар');
                $xmlOrderPart->addChild('UUID', $orderOffer->offer_spare_uuid);
                $xmlOrderPart->addChild('Наименование', $orderOffer->offer_spare_title);
                $xmlOrderPart->addChild('Количество', $orderOffer->quantity);
                $xmlOrderPart->addChild('Цена', sprintf('%.2f', $orderOffer->offer_price));
                $xmlOrderPart->addChild('Сумма', sprintf('%.2f', $orderOffer->price));
                $xmlOrderPart->addChild('ПроцентСкидки', 0);
                $xmlOrderPart->addChild('ИтогоПоСтроке', sprintf('%.2f', $orderOffer->price));

                $xmlTax = $xmlOrderPart->addChild('Налог');
                $xmlTax->addChild('СтавкаНДС', 0);
                $xmlTax->addChild('УчтеноВСумме', 'true');
                $xmlTax->addChild('СуммаНДС', 0);

                $xmlSupplier = $xmlOrderPart->addChild('Поставщик');
                $xmlSupplier->addChild('UUID', $orderOffer->offer_supplier_uuid);
                $xmlSupplier->addChild('Наименование', $orderOffer->offer_supplier_title);
            }
        }

        $xmlTransactions = $xml->addChild('Платежи');
        foreach ($transactions as $transaction) {
            /** @var $transaction Transactions */
            $xmlTransaction = $xmlTransactions->addChild('ДокументОплаты');
            $xmlTransaction->addChild('UUID', $transaction->external_id);
            $xmlTransaction->addChild('ИдСайта', $this->generateId($transaction, $transaction->created_at, $transaction->id));
            $xmlTransaction->addChild('НомерДокумента', $transaction->id);
            $xmlTransaction->addChild('ДатаДокумента', $this->date(strtotime($transaction->created_at)));
            $xmlTransaction->addChild('Удален', 'false');
            $xmlTransaction->addChild('Сумма', sprintf('%.2f', $transaction->amount));
            $xmlTransaction->addChild('Валюта', 'RUB');

            switch($transaction->status) {
                case Transactions::STATUS_PENDING:
                    $xmlTransaction->addChild('СтатусОплаты', 'В ожидании');
                break;
                case Transactions::STATUS_DONE:
                    $xmlTransaction->addChild('СтатусОплаты', 'Выполнено');
                break;
            }

            $xmlTransaction->addChild('ОписаниеПлатежа', 'По заказу #' . $transaction->order_id . ', пополнение баланса');

            $xmlTransactionClient = $xmlTransaction->addChild('Клиент');
            $xmlTransactionClient->addChild('UUID', $transaction->client->external_id);
            $xmlTransactionClient->addChild('ИдСайта', $this->generateId($transaction->client, $transaction->client->created_at, $transaction->client->id));
            $xmlTransactionClient->addChild('Наименование', $transaction->client->stringFor1c());
        }

        $doc = new DOMDocument('1.0');
        $doc->preserveWhiteSpace = false;
        $doc->loadXML($xml->asXML());
        $doc->formatOutput = true;

        echo $doc->saveXML();
    }

    public function actionSuccess()
    {
        if (Orders::model()->for1c()->count() > 0) {
            /** @var $order Orders */
            foreach (Orders::model()->for1c()->findAll() as $order) {
                $order->send_to_1c = false;
                $order->save();
            }
        }

        if (Transactions::model()->for1c()->count() > 0) {
            /** @var $transaction Transactions */
            foreach (Transactions::model()->for1c()->findAll() as $transaction) {
                $transaction->send_to_1c = false;
                $transaction->save();
            }
        }

        if (Clients::model()->for1c()->count() > 0) {
            /** @var $client Clients */
            foreach (Clients::model()->for1c()->findAll() as $client) {
                $client->send_to_1c = false;
                $client->save();
            }
        }

        print "success";
    }
}
