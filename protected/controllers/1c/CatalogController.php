<?php

class CatalogController extends My1cController
{
    private $_dir;

    public function init()
    {
        $this->_dir = dirname(__FILE__) . '/../../../uploads/1c/';
        parent::init();
    }

    public function actionCheckAuth()
    {
        $cookieName = MyString::random(16);
        $cookieValue = MyString::random(32);

        Yii::app()->getCache()->set('1cCookieName', $cookieName, 600);
        Yii::app()->getCache()->set('1cCookieValue', $cookieValue, 600);

        printf("%s\n%s\n%s", 'success', $cookieName, $cookieValue);
    }

    public function actionInit()
    {
        $limit = ini_get('post_max_size');

        switch (strtolower(substr($limit, -1))) {
            case 'm':
                $limit = (int)$limit * 1048576;
                break;

            case 'k':
                $limit = (int)$limit * 1024;
                break;

            case 'g':
                $limit = (int)$limit * 1073741824;
                break;

            default:
                $limit = (int)$limit;
                break;
        }

        printf("zip=no\nfile_limit=%s", $limit);
    }

    public function actionFile($filename)
    {
        $f = fopen($this->_dir . $filename, 'w');
        fwrite($f, file_get_contents('php://input'));
        fclose($f);
        print "success\n";
    }

    public function actionImport($filename)
    {
        Yii::import('application.modules.clients.models.*');

        if($filename == 'import.xml') {
            $z = new XMLReader;
            $z->open($this->_dir . $filename);

            while($z->read() && $z->name != 'Каталоги');
            $xml = new SimpleXMLElement($z->readOuterXML());
            $z->close();

            if (isset($xml->Поставщики)) {
                foreach ($xml->Поставщики->Поставщик as $xmlSupplier) {
                    $supplier = Suppliers::model()->findByAttributes(array('external_id' => $xmlSupplier->UUID));
                    if ($supplier !== null) {
                        $supplier->delete();
                    }
                    $supplier = new Suppliers();
                    $supplier->external_id = $xmlSupplier->UUID;
                    $supplier->title = $xmlSupplier->Наименование;
                    $rating = $xmlSupplier->Рейтинг;
                    if ($rating < 1) {
                        $rating = 1;
                    }
                    if ($rating > 100) {
                        $rating = 100;
                    }
                    $supplier->rating = $rating;
                    $supplier->delivery_time = $xmlSupplier->СрокПоставки;
                    $supplier->save();
                }
            }

            if (isset($xml->Производители)) {
                foreach ($xml->Производители->Производитель as $xmlManufacturer) {
                    $manufacturer = Manufacturers::model()->findByAttributes(array('external_id' => $xmlManufacturer->UUID));
                    if ($manufacturer == null) {
                        $manufacturer = new Manufacturers();
                        $manufacturer->external_id = $xmlManufacturer->UUID;
                    }
                    $manufacturer->title = $xmlManufacturer->Наименование;
                    $manufacturer->title_full = $xmlManufacturer->ПолноеНаименование;
                    $manufacturer->site = $xmlManufacturer->Сайт;
                    $manufacturer->save();
                }
            }

            if (isset($xml->Модели)) {
                foreach ($xml->Модели->Модель as $xmlModel) {
                    $model = Models::model()->findByAttributes(array('external_id' => $xmlModel->UUID));
                    if ($model == null) {
                        $model = new Models();
                        $model->external_id = $xmlModel->UUID;
                    }
                    $model->title = $xmlModel->Наименование;
                    if ($xmlModel->Производитель->UUID) {
                        $model->manufacturer_id = Manufacturers::model()->findByAttributes(array('external_id' => $xmlModel->Производитель->UUID))->id;
                    }
                    $model->years = $xmlModel->ГодыВыпуска;
                    $model->engineType = $xmlModel->ХарактеристикиДвигателя->ДвигательВид;
                    $model->engineFuelSupply = $xmlModel->ХарактеристикиДвигателя->ДвигательПодачаТоплива;
                    $model->enginePower = $xmlModel->ХарактеристикиДвигателя->ДвигательМощность;
                    $model->engineVolume = $xmlModel->ХарактеристикиДвигателя->ДвигательОбъем;
                    $model->engineCylinders = $xmlModel->ХарактеристикиДвигателя->ДвигательЦилиндров;
                    $model->engineValvesPerCylinder = $xmlModel->ХарактеристикиДвигателя->ДвигательКлапановНаЦилиндр;
                    $model->engineCode = $xmlModel->ХарактеристикиДвигателя->ДвигательКод;
                    $model->enforcementBodies = $xmlModel->ИсполнениеКузова;
                    $model->suspensionType = $xmlModel->Подвеска->ВидПривода;
                    $model->suspensionAxisConfiguration = $xmlModel->Подвеска->КонфигурацияОси;
                    $model->suspensionTonnage = $xmlModel->Подвеска->Тоннаж;
                    $model->brakingType = $xmlModel->ХарактеристикиТормознойСистемы->ВидТормознойСистемы;
                    $model->brakingABS = $xmlModel->ХарактеристикиТормознойСистемы->ABS;
                    $model->brakingASR = $xmlModel->ХарактеристикиТормознойСистемы->ASR;
                    $model->save();
                }
            }

            if(isset($xml->Товары->Группа)) {
                foreach ($xml->Товары->Группа as $xmlGroup) {
                    $spareCategory = SpareCategories::model()->findByAttributes(array('external_id' => $xmlGroup->UUID));
                    if ($spareCategory == null) {
                        $spareCategory = new SpareCategories();
                        $spareCategory->external_id = $xmlGroup->UUID;
                    }
                    if ($xmlGroup->Родитель->UUID) {
                        $spareCategory->parent_id = SpareCategories::model()->findByAttributes(array('external_id' => $xmlGroup->Родитель->UUID))->id;
                    }
                    $spareCategory->title = $xmlGroup->Наименование;
                    $spareCategory->save();
                }
            }

            if (isset($xml->Товары)) {
                foreach ($xml->Товары->Товар as $xmlSpare) {
                    $spare = Spares::model()->findByAttributes(array('external_id' => $xmlSpare->UUID));
                    if ($spare == null) {
                        $spare = new Spares();
                        $spare->external_id = $xmlSpare->UUID;
                    }
                    if ($xmlSpare->Родитель->UUID) {
                        $spare->parent_id = Spares::model()->findByAttributes(array('external_id' => $xmlSpare->Родитель->UUID))->id;
                    }
                    $spare->manufacturer_id = Manufacturers::model()->findByAttributes(array('external_id' => $xmlSpare->Производитель))->id;
                    $spare->articul = $xmlSpare->Артикул;
                    $spare->title = $xmlSpare->Наименование;
                    $spare->title_eng = $xmlSpare->НаименованиеИностранное;
                    $spare->unit = $xmlSpare->Единица;

                    $spareModels = array();
                    if ($xmlSpare->Применяемость) {
                        foreach($xmlSpare->Применяемость->Модель as $xmlSpareModel) {
                            $spareModel = Models::model()->findByAttributes(array('external_id' => $xmlSpareModel->UUID));
                            if ($spareModel !== null) {
                                $spareModels[] = $spareModel->id;
                            }
                        }
                    }
                    $spare->models = $spareModels;

                    $spare->weight = $xmlSpare->Вес;
                    $spare->volume = $xmlSpare->Объем;
                    $spare->comment = $xmlSpare->Комментарий;
                    $spare->pre_packing = $xmlSpare->Фасовка;
                    $spare->vat_rate = $xmlSpare->Налог->СтавкаНДС;
                    $spare->vat_in_the_amount = $xmlSpare->Налог->УчтеноВСумме;
                    $spare->picture = $xmlSpare->Картинка;

                    $spare->save();

                    if ($xmlSpare->Характеристики) {
                        SpareParameterValues::model()->deleteAllByAttributes(array('spare_id' => $spare->id));
                        foreach($xmlSpare->Характеристики->Характеристика as $xmlSpareParameter) {
                            $spareParameter = SpareParameters::model()->findByAttributes(array('title' => $xmlSpareParameter->Параметр));
                            if ($spareParameter == null) {
                                $spareParameter = new SpareParameters();
                                $spareParameter->title = $xmlSpareParameter->Параметр;
                                $spareParameter->save();
                            }
                            $spareParameterValue = new SpareParameterValues();
                            $spareParameterValue->spare_id = $spare->id;
                            $spareParameterValue->parameter_id = $spareParameter->id;
                            $spareParameterValue->value = $xmlSpareParameter->Значение;
                            $spareParameterValue->save();
                        }
                    }
                }

                foreach ($xml->Товары->Товар as $xmlSpare) {
                    if (isset($xmlSpare->Аналоги)) {
                        /** @var $currentSpare Spares */
                        $currentSpare = Spares::model()->findByAttributes(array('external_id' => $xmlSpare->UUID));
                        $analogs = array();
                        foreach($xmlSpare->Аналоги as $xmlAnalog) {
                            $analog = Spares::model()->findByAttributes(array('external_id' => $xmlAnalog->Аналог));
                            if ($analog !== null) {
                                $analogs[] = $analog->id;
                            }
                        }
                        $currentSpare->analogs = $analogs;
                        $currentSpare->save();
                    }
                }
            }

            if (isset($xml->ИсточникиИнформации)) {
                foreach ($xml->ИсточникиИнформации->ИсточникИнформации as $xmlInformationSource) {
                    $informationSource = InformationSources::model()->findByAttributes(array('external_id' => $xmlInformationSource->UUID));
                    if ($informationSource == null) {
                        $informationSource = new InformationSources();
                        $informationSource->external_id = $xmlInformationSource->UUID;
                    }
                    $informationSource->title = $xmlInformationSource->Наименование;
                    $informationSource->save();
                }
            }

            if (isset($xml->Банки)) {
                foreach ($xml->Банки->Банк as $xmlBank) {
                    $bank = Banks::model()->findByAttributes(array('external_id' => $xmlBank->UUID));
                    if ($bank == null) {
                        $bank = new Banks();
                        $bank->external_id = $xmlBank->UUID;
                    }
                    $bank->title = $xmlBank->Наименование;
                    $bank->bik = $xmlBank->БИК;
                    $bank->city = $xmlBank->Город;
                    $bank->correspondent_account = $xmlBank->КоррСчет;
                    $bank->save();
                }
            }

            if (isset($xml->Контрагенты)) {
                foreach ($xml->Контрагенты->Клиент as $xmlClient) {
                    /** @var $client Clients */
                    $client = null;
                    if ($xmlClient->ИдСайта) {
                        $client = Clients::model()->findByPk($xmlClient->ИдСайта);
                    }
                    if ($client == null) {
                        $client = Clients::model()->findByAttributes(array('external_id' => $xmlClient->UUID));
                    }
                    if ($client == null) {
                        $client = new Clients();
                    }
                    $client->external_id = $xmlClient->UUID;
                    $client->title = $xmlClient->Наименование;
                    $xmlClientType = $xmlClient->ТипКлиента;
                    switch($xmlClientType) {
                        case 'ЮридическоеЛицо':
                            $client->type = Clients::TYPE_COMPANY;
                        break;

                        case 'ЧастныйПредприниматель':
                            $client->type = Clients::TYPE_PRIVATE_ENTREPRENEUR;
                        break;

                        case 'ЧастноеЛицо':
                            $client->type = Clients::TYPE_PRIVATE;
                        break;
                    }
                    $client->discount = $xmlClient->РазмерСкидки;
                    if ($xmlClient->ИсточникИнформации) {
                        $client->information_source_id = InformationSources::model()->findByAttributes(array('external_id' => $xmlClient->ИсточникИнформации))->id;
                    }
                    $client->last_name = $xmlClient->КонтактноеЛицо->Фамилия;
                    $client->first_name = $xmlClient->КонтактноеЛицо->Имя;
                    $client->patronymic_name = $xmlClient->КонтактноеЛицо->Отчество;

                    $client->phone['country_code'] = $xmlClient->КонтактныйТелефон->КодСтраны;
                    $client->phone['city_code'] = $xmlClient->КонтактныйТелефон->КодГорода;
                    $client->phone['phone'] = $xmlClient->КонтактныйТелефон->Номер;
                    $client->phone['internal'] = $xmlClient->КонтактныйТелефон->ВнутреннийНомер;

                    $client->email = $xmlClient->ЭлектроннаяПочта;
                    $client->post_address_zip = $xmlClient->ПочтовыйАдрес->Индекс;
                    $client->post_address_city = $xmlClient->ПочтовыйАдрес->Город;
                    $client->post_address_street = $xmlClient->ПочтовыйАдрес->Улица;
                    $client->post_address_house = $xmlClient->ПочтовыйАдрес->Дом;
                    $client->post_address_housing = $xmlClient->ПочтовыйАдрес->Корпус;
                    $client->post_address_room = $xmlClient->ПочтовыйАдрес->КвартираОфис;
                    $client->comments = $xmlClient->ДополнительнаяИнформация;

                    $client->organization = $xmlClient->ХарактеристикиЮрЛица->Наименование;
                    $client->inn = $xmlClient->ХарактеристикиЮрЛица->ИНН;
                    $client->kpp = $xmlClient->ХарактеристикиЮрЛица->КПП;
                    $client->ogrnip = $xmlClient->ХарактеристикиЮрЛица->ОГРН;
                    $client->bank_rs = $xmlClient->ХарактеристикиЮрЛица->БанковскиеРеквизиты->РасчетныйСчет;
                    $client->bank_bik = $xmlClient->ХарактеристикиЮрЛица->БанковскиеРеквизиты->Банк[0]->БИК;
                    $client->bank_correspondent_account = $xmlClient->ХарактеристикиЮрЛица->БанковскиеРеквизиты->Банк[0]->КоррСчет;
                    $client->legal_address_zip = $xmlClient->ХарактеристикиЮрЛица->ЮридическийАдрес->Индекс;
                    $client->legal_address_city = $xmlClient->ХарактеристикиЮрЛица->ЮридическийАдрес->Город;
                    $client->legal_address_street = $xmlClient->ХарактеристикиЮрЛица->ЮридическийАдрес->Улица;
                    $client->legal_address_house = $xmlClient->ХарактеристикиЮрЛица->ЮридическийАдрес->Дом;
                    $client->legal_address_housing = $xmlClient->ХарактеристикиЮрЛица->ЮридическийАдрес->Корпус;
                    $client->legal_address_room = $xmlClient->ХарактеристикиЮрЛица->ЮридическийАдрес->КвартираОфис;
                }
            }

            if (isset($xml->Доставщики)) {
                foreach ($xml->Доставщики->СлужбаДоставки as $xmlDelivery) {
                    $delivery = Deliveries::model()->findByAttributes(array('external_id' => $xmlDelivery->UUID));
                    if ($delivery == null) {
                        $delivery = new Deliveries();
                        $delivery->external_id = $xmlDelivery->UUID;
                    }
                    $delivery->title = $xmlDelivery->Наименование;
                    $delivery->save();
                }
            }

            print "success";
            unlink($this->_dir . $filename);
        } else if($filename == 'offers.xml') {
            $z = new XMLReader;
            $z->open($this->_dir . $filename);

            while($z->read() && $z->name != 'ПакетПредложений');
            $xml = new SimpleXMLElement($z->readOuterXML());
            $z->close();

            foreach($xml->Предложения->Предложение as $xmlOffer) {
                $spare = Spares::model()->findByAttributes(array('external_id' => $xmlOffer->Товар->UUID));
                if ($spare !== null) {
                    Offers::model()->deleteAllByAttributes(array('spare_id' => $spare->id));

                    foreach($xmlOffer->Поставщики->ПредложениеПоставщика as $xmlSupplierOffers) {
                        $supplier = Suppliers::model()->findByAttributes(array('external_id' => $xmlSupplierOffers->Поставщик->UUID));
                        if ($supplier != null) {
                            $offer = Offers::model()->findByAttributes(array('spare_id' => $spare->id, 'supplier_id' => $supplier->id));
                            if ($offer == null) {
                                $offer = new Offers();
                                $offer->spare_id = $spare->id;
                                $offer->supplier_id = $supplier->id;
                            }
                            $offer->price = $xmlSupplierOffers->Цена;
                            $offer->availability = $xmlSupplierOffers->Количество;
                            $offer->multiplicity_of_order = $xmlSupplierOffers->Кратность;
                            $offer->save();
                        }
                    }
                }
            }

            foreach($xml->Балансы->БалансКлиента as $xmlClientBalance) {
                /** @var $client Clients */
                $client = Clients::model()->findByAttributes(array('external_id' => $xmlClientBalance->Клиент->UUID));
                $client->balance = $xmlClientBalance->БалансНаКонец;
                $client->save();

                foreach($xmlClientBalance->ДвиженияДенежныхСредств->Операция as $xmlClientTransaction) {
                    $transaction = new Transactions();
                    $transaction->client_id = $client->id;
                    $transaction->amount = $xmlClientTransaction->СуммаОперации;
                    switch ($xmlClientTransaction->ВидОперации) {
                        case 'Приход':
                            $transaction->type = Transactions::TYPE_DEBIT;
                        break;

                        case 'Расход':
                            $transaction->type = Transactions::TYPE_CREDIT;
                        break;
                    }
                    $transaction->status = Transactions::STATUS_DONE;
                    $transaction->send_to_1c = false;
                    $transaction->save();
                }
            }

            print "success";
            unlink($this->_dir . $filename);
        } else if($filename == 'orders.xml') {
            $z = new XMLReader;
            $z->open($this->_dir . $filename);

            while($z->read() && $z->name != 'КоммерческаяИнформация');
            $xml = new SimpleXMLElement($z->readOuterXML());
            $z->close();

            foreach($xml->ЗаказПокупателя as $xmlOrder) {
                $order = null;
                if ($xmlOrder->ИдСайта) {
                    $order = Orders::model()->findByPk($xmlOrder->ИдСайта);
                }
                if ($order == null) {
                    $order = Orders::model()->findByAttributes(array('external_id' => $xmlOrder->UUID));
                }
                if ($order == null) {
                    $order = new Orders();
                }
                $order->external_id = $xmlOrder->UUID;
                $order->client_id = $xmlOrder->Клиент->UUID;

                $statuses = array_flip($order->statusesFor1c());
                $order->status = $statuses[$xmlOrder->СтатусЗаказа->ЗначениеСтатуса];
                $order->price = $xmlOrder->СуммаДокумента;

                if ($xmlOrder->Доставка) {
                    $delivery = Deliveries::model()->findByAttributes(array('external_id' => $xmlOrder->Доставка->СлужбаДоставки->UUID));
                    if ($delivery) {
                        $order->delivery_id = $delivery->id;
                    }
                }

                $order->save();

                OrderOffers::model()->deleteAllByAttributes(array('order_id' => $order->id));
                foreach($xmlOrder->Товары as $xmlOrderOffer) {

                }
            }

            print "success";
            unlink($this->_dir . $filename);
        }
    }
}
