<?php

class EGallery extends CWidget
{
    public $model;
    public $attribute;
    public $picturesController;

    public function run()
    {
        echo MyHtml::label('Фотогалерея', false, array('class' => 'control-label'));

        if (!$this->model->isNewRecord) {
            echo '<div class="controls">';

            $this->widget('application.extensions.EGallery.EAjaxUpload.EAjaxUpload', array(
                // 'id' => 'upload_files',
                'config' => array(
                    'action' => $this->getController()->createUrl('uploadGalleryPicture', array('pk' => $this->model->id)),
                    'multiple' => true,
                ),
            ));

            $picturesProvider = new CArrayDataProvider($this->model->{$this->attribute}, array('pagination' => false));
            $this->widget('application.extensions.EGallery.EGalleryGridView', array(
                // 'id' => 'gallery-grid',
                'dataProvider' => $picturesProvider,
                'columns' => array(
                    'picture',
                    array(
                        'class' => 'CButtonColumn',
                        'template' => '{view} {delete}',
                        'deleteButtonUrl' =>
                            'Yii::app()->controller->createUrl("'.$this->picturesController.'/delete", array("id" => $data->primaryKey))',
                        'viewButtonUrl' => '$data->uploadTo(\'picture\')',
                        'viewButtonOptions' => array('class' => 'view', 'rel' => 'gallery-grid-images'),
                    ),
                ),
            ));

            $this->widget('application.extensions.EColorBox.EColorBox', array(
                'target' => 'a.view',
            ));

            echo '</div>';
        } else {
            echo 'Возможность будет доступна после создания записи';
        }
    }
}
