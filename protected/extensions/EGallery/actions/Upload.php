<?php

Yii::import('application.extensions.EFileUploadAction.EFileUploadAction');

class Upload extends CAction
{
    public $model;
    public $refAttribute;

    public function run()
    {
        Yii::import('application.extensions.EGallery.EAjaxUpload.FileUploader');

        if (empty($this->model) || empty($this->refAttribute)) {
            echo json_encode(array('error' => 'Один из свойств класса не определен.'));
        } else {
            $pk = $this->getController()->getQuery('pk');

            $page_pictures = new $this->model;
            $page_pictures->{$this->refAttribute} = $pk;

            $uploader = new FileUploader(array('jpeg', 'jpg', 'gif', 'png'));
            $dir = $page_pictures->uploadTo('picture');
            $result = $uploader->handleUpload(MyFilesystem::makeDirs($dir));

            if (isset($result['success']) && $result['success'] === true) {
                $page_pictures->picture = $result['filename'];
                $page_pictures->save();
            }

            echo json_encode($result);
        }
    }
}
