<?php

Yii::import('zii.widgets.grid.CGridView');

class EGalleryGridView extends CGridView
{
    public function init()
    {
        $this->hideHeader = true;
        $this->selectableRows = false;
        $this->template = '{items}';
        parent::init();
    }
}
