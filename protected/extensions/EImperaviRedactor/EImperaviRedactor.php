<?php
/**
 * ImperaviRedactorWidget wrapper around {@link http://imperavi.ru/redactor/ imperavi WYSIWYG}.
 *
 * Usage:
 * <pre>
 * $this->widget('application.extensions.EImperaviRedactor.EImperaviRedactor', array(
 *     'model' => $model,
 *     'attribute' => $field,
 *     'options' => array(
 *         'toolbar' => 'main',
 *     ),
 *     'htmlOptions' => array('style' => 'width: 100%; height: 200px;'),
 * ));
 * </pre>
 */
class EImperaviRedactor extends CInputWidget
{
    /**
     * @var string
     */
    public $packageName = 'imperavi-redactor';
    /**
     * @var array imperavi redactor {@link http://imperavi.ru/redactor/docs/ options}.
     */
    public $options = array();

    /**
     * Init widget.
     */
    public function init()
    {
        list($this->name, $this->id) = $this->resolveNameId();
        $this->getPackage();
    }

    /**
     * Run widget.
     */
    public function run()
    {
        if (isset($this->options['path']))
            $this->options['path'] = rtrim($this->options['path'], '/\\') . '/';

        $this->registerClientScript();

        if ($this->hasModel())
            echo CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
        else
            echo CHtml::textArea($this->name, $this->value, $this->htmlOptions);
    }

    /**
     * @return array
     */
    public function getPackage()
    {
        /**
         * @var CClientScript $cs
         * @var CAssetManager $ap
         */
        $cs = Yii::app()->getClientScript();
        $ap = Yii::app()->getAssetManager();

        if (!isset($cs->packages[$this->packageName]))
            $cs->packages[$this->packageName] = array(
                'basePath' => dirname(__FILE__) . '/assets',
                'js' => array('redactor.js',),
                'css' => array('css/redactor.css',),
                'depends' => array('jquery',),
            );

        // Publish package assets. Force copy assets in debug mode.
        if (!isset($cs->packages[$this->packageName]['baseUrl']))
            $cs->packages[$this->packageName]['baseUrl'] = $ap->publish($cs->packages[$this->packageName]['basePath'], false, -1, YII_DEBUG);

        return $cs->packages[$this->packageName];
    }

    /**
     * Register CSS and Script.
     */
    protected function registerClientScript()
    {
        /**
         * @var CClientScript $cs
         */
        $cs = Yii::app()->getClientScript();
        $cs->registerPackage($this->packageName);
        $cs->registerScript(
            __CLASS__ . '#' . $this->getId(),
            'jQuery(' . CJavaScript::encode('#' . $this->getId()) . ').redactor(' . CJavaScript::encode($this->options) . ');',
            CClientScript::POS_READY
        );
    }
}
