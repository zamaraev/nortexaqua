<?php

class MyHtml extends CHtml
{
    public static function button($label = 'button', $htmlOptions = array())
    {
        if (!isset($htmlOptions['class'])) {
            $htmlOptions['class'] = '';
        }
        $htmlOptions['class'] = trim($htmlOptions['class'] . ' btn');

        return parent::button($label, $htmlOptions);
    }

    public static function submitButton($label = 'submit', $htmlOptions = array())
    {
        if (!isset($htmlOptions['class'])) {
            $htmlOptions['class'] = '';
        }
        $htmlOptions['class'] = trim($htmlOptions['class'] . ' btn btn-primary');

        return parent::submitButton($label, $htmlOptions);
    }

    public static function frontendUrlsList()
    {
        Yii::import('application.modules.settings.models.*');

        $results = array();

        $results['Основное'] = array(
            CJSON::encode(array('/')) => 'Главная',
        );

        $results['Страницы'] = array();
        foreach (SettingsFrontendRoutes::model()->findAll() as $page) {
            $pageId = CJSON::encode(array('/pages/default/show', array('id' => $page->id)));
            $results['Страницы'][$pageId] = $page->__toString();
        }

        return $results;
    }

//    public static function frontendUrlFromJson($params)
//    {
//        $menuItemUrl = CJSON::decode($params);
//
//        $menuItemRoute = '/';
//        if ($menuItemUrl !== null) {
//            $menuItemRoute = $menuItemUrl[0];
//        }
//
//        $menuItemParams = array();
//        if (isset($menuItemUrl[1])) {
//            $menuItemParams = $menuItemUrl[1];
//        }
//
//        return Yii::app()->getController()->createUrl($menuItemRoute, $menuItemParams);
//    }

    public static function frontendPathNameFromJson($params)
    {
        $urlList = self::frontendUrlsList();
        foreach ($urlList as $urlGroup => $urls) {
            foreach ($urls as $urlParams => $urlTitle) {
                if ($urlParams == $params) {
                    return $urlGroup . ' &raquo; ' . $urlTitle;
                }
            }
        }
        return null;
    }
}
