<?php

class MyBackendWebUser extends CWebUser
{
    public function getModel()
    {
        return $this->_model();
    }

    /**
     * @return BackendUsers
     */
    private function _model()
    {
        return BackendUsers::model()->findByAttributes(array('username' => $this->getId()));
    }
}
