<?php

class MyFrontendWebUser extends CWebUser
{
    public function getRegion()
    {
        return $this->getState('region');
    }

    public function setRegion($id)
    {
        $this->setState('region', $id);
    }

    public function getDelivery()
    {
        return $this->getState('delivery');
    }

    public function setDelivery($id)
    {
        $this->setState('delivery', $id);
    }

    public function getAddress()
    {
        return $this->getState('address');
    }

    public function setAddress($array)
    {
        $this->setState('address', $array);
    }

    public function getBalance()
    {
        return floatval($this->_model()->balance);
    }

    public function getModel()
    {
        return $this->_model();
    }

    /**
     * @return Clients
     */
    private function _model()
    {
        return Clients::model()->findByPk($this->getId());
    }
}
