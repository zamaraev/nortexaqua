<?php

class MyWebModule extends CWebModule
{
    protected function init()
    {
        Yii::app()->onModuleCreate(new CEvent($this));
    }
}
