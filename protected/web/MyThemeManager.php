<?php

class MyThemeManager extends CThemeManager
{
    public function init()
    {
        $this->themeClass = 'MyTheme';
        parent::init();
    }
}
