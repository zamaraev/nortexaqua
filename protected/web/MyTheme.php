<?php

class MyTheme extends CTheme
{
    public function __construct($name, $basePath, $baseUrl)
    {
        $basePath .= DIRECTORY_SEPARATOR . Yii::app()->endName;
        $baseUrl .= DIRECTORY_SEPARATOR . Yii::app()->endName;
        parent::__construct($name, $basePath, $baseUrl);
    }
}
