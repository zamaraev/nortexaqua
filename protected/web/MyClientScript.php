<?php

class MyClientScript extends CClientScript
{
    public function registerLessFile($url)
    {
        return parent::registerLinkTag('stylesheet/less', 'text/css', $url);
    }
}
