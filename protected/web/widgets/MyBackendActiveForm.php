<?php

class MyBackendActiveForm extends CActiveForm
{
    public function init()
    {
        $this->htmlOptions['class'] = 'form-horizontal';
        if (!isset($this->htmlOptions['onSubmit'])) {
            $this->htmlOptions['onSubmit'] = 'js:$("input[type=submit]").attr("disabled", true); return true;';
        }
        parent::init();
    }

    public function renderField($model, $field)
    {
        $modelReflection = new ReflectionClass($model);
        if ($modelReflection->hasProperty($field)) {
            if (is_array($model->$field)) {
                return $this->myDropDownList($model, $field, $model->$field);
            } else {
                return $this->myTextField($model, $field);
            }
        }
        return $this->generateField($model, $field);
    }

//    public function renderFileField($model, $field) {
//        $return = $this->myFileField($model, $field);
//
//        return $return;
//    }

//    public function renderAutoCompleteField($model, $field, $sourceUrl) {
//        return $this->myAutoCompleteField($model, $field, $sourceUrl);
//    }

    public function generateField($model, $field)
    {
        if (isset($model->getMetaData()->columns[$field]) && !$model->getMetaData()->columns[$field]->isForeignKey) {
            $modelField = $model->getMetaData()->columns[$field];
        } else if (isset($model->getMetaData()->relations[$field])) {
            $modelField = $model->getMetaData()->relations[$field];
        }

        if (!isset($modelField)) {
            throw new CException(
                '"' . $field . '" не найден в модели "' . get_class($model) . '" либо является primary/foreign ключом'
            );
        }

        switch (get_class($modelField)) {
            case 'CManyManyRelation':
                $return = $this->myListBox($model, $modelField->name);
                break;

            case 'CBelongsToRelation':
                $return = $this->myDropDownList($model, $modelField->name);
                break;

            case 'CHasOneRelation':
                throw new CException('CHasOneRelation не реализован');
                break;

            case 'CHasManyRelation':
                throw new CException('CHasManyRelation не реализован');
                break;

            default:
                switch ($modelField->type) {
                    case 'boolean':
                        $return = $this->myCheckBox($model, $modelField->name);
                        break;

                    default:
                        if ($model->isAttributeEnumerable($modelField->name)) {
                            $return = $this->myDropDownList($model, $modelField->name);
                        }

                        foreach ($model->getValidators($modelField->name) as $validator) {
                            if ($validator instanceof CFileValidator) {

                                $return = $this->myFileField($model, $modelField->name);
                            }
                        }

                        if (!isset($return)) {
                            switch ($modelField->dbType) {
                                case 'text':
                                    $return = $this->myTextArea($model, $modelField->name);
                                    break;

                                case 'date':
                                    $return = $this->myDateField($model, $modelField->name);
                                    break;

                                case 'time without time zone':
                                    $return = $this->myDateTimeField($model, $modelField->name, true);
                                    break;

                                case 'timestamp without time zone':
                                    $return = $this->myDateTimeField($model, $modelField->name);
                                    break;

                                default:
                                    $return = $this->myTextField($model, $modelField->name);
                                    break;
                            }
                        }
                        break;
                }
                break;
        }

        return $return;
    }

//    public function myDayOfWeek($model, $field) {
//        $html = $this->myDropDownList($model, $field, array(
//            2 => Yii::t('daysOfWeek', 'Mon'),
//            3 => Yii::t('daysOfWeek', 'Tue'),
//            4 => Yii::t('daysOfWeek', 'Wed'),
//            5 => Yii::t('daysOfWeek', 'Thu'),
//            6 => Yii::t('daysOfWeek', 'Fri'),
//            7 => Yii::t('daysOfWeek', 'Sat'),
//            1 => Yii::t('daysOfWeek', 'Sun'),
//        ));
//
//        return $html;
//    }

    public function myTextField($model, $field, $options = array())
    {
        if ($model instanceof MyActiveRecord && isset($model->getMetaData()->columns[$field])) {
            $modelColumnMd = $model->getMetaData()->columns[$field];
            switch ($modelColumnMd->type) {
                case 'string':
                    if ($modelColumnMd->size > 60) {
                        $_size = 60;
                    } else if ($modelColumnMd->size < 3) {
                        $_size = 3;
                    } else {
                        $_size = $modelColumnMd->size;
                    }
                    $_maxlength = $modelColumnMd->size;
                    break;

                case 'integer':
                case 'double':
                    $_size = 3;
                    $_maxlength = 100;
                    break;
            }
        } else {
            $_size = 60;
            $_maxlength = 100;
        }

        $html = $this->labelEx($model, $field);
        $html .= $this->beginControls();

        $htmlOptions = array_merge(
            array('size' => $_size, 'maxlength' => $_maxlength),
            $options
        );

        if (strstr($field, 'password')) {
            $html .= $this->passwordField($model, $field, $htmlOptions);
        } else {
            $html .= $this->textField($model, $field, $htmlOptions);
        }

        $html .= $this->error($model, $field);
        $html .= $this->endControls();

        return $html;
    }

    public function myTextEditorField($model, $field)
    {
        $app = Yii::app();

        $html = $this->widget('application.extensions.EImperaviRedactor.EImperaviRedactor', array(
            'model' => $model,
            'attribute' => $field,
            'options' => array(
                'toolbar' => 'my',
                'image_upload' => $app->createUrl($app->getController()->getModule()->getId() . '/' . $app->getController()->getId() . '/upload'),
            ),
            'htmlOptions' => array('style' => 'width: 100%; height: 200px;'),
        ), true);

        return $html;
    }

    public function myDateField($model, $field)
    {
        $html = $this->labelEx($model, $field);
        $html .= $this->beginControls();
        $html .= $this->widget('application.zii.widgets.jui.MyJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => $field
        ), true);
        $html .= $this->error($model, $field);
        $html .= $this->endControls();
        return $html;
    }

    public function myDateTimeField($model, $field, $timePickerOnly = false)
    {
        $html = $this->labelEx($model, $field);
        $html .= $this->beginControls();
        $html .= $this->widget('application.zii.widgets.jui.MyJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => $field,
            'timePickerOnly' => $timePickerOnly,
            'options' => array(
                'hourGrid' => 4,
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'dd.mm.yy',
            ),
        ), true);
        $html .= $this->error($model, $field);
        $html .= $this->endControls();
        return $html;
    }

    public function myPhoneField($model, $field)
    {
        $html = $this->labelEx($model, $field);
        $html .= $this->beginControls();
        $html .= $this->widget('application.web.widgets.MyMaskedTextField', array(
            'model' => $model,
            'attribute' => $field,
            'mask' => '+7 (999) 999-99-99'
        ), true);
        $html .= $this->error($model, $field);
        $html .= $this->endControls();
        return $html;
    }

    public function myTextArea($model, $field, $htmlOptions = array())
    {
        $html = $this->labelEx($model, $field);
        $html .= $this->beginControls();
        $html .= $this->textArea($model, $field, array_merge(array('rows' => 8, 'style' => 'width: 95%'), $htmlOptions));
        $html .= $this->error($model, $field);
        $html .= $this->endControls();
        return $html;
    }

    public function myCheckBox($model, $field)
    {
        $html = $this->labelEx($model, $field);
        $html .= $this->beginControls();
        $html .= $this->checkBox($model, $field);
        $html .= $this->error($model, $field);
        $html .= $this->endControls();
        return $html;
    }

    public function myListBox($model, $field, $group = '', $data = null)
    {
        $relationClassName = $model->getMetaData()->relations[$field]->className;
        if ($data === null) {
            $data = MyHtml::listData(
                call_user_func(array($relationClassName, 'model'))->findAll(),
                call_user_func(array($relationClassName, 'model'))->getTableSchema()->primaryKey,
                'toString',
                $group
            );
        }

        $html = $this->labelEx($model, $field);
        $html .= $this->beginControls();
        $html .= $this->widget('application.zii.widgets.jui.MyMultiSelect', array(
            'model' => $model,
            'attribute' => $field,
            'data' => $data,
            'htmlOptions' => array('class' => 'multiselect')
        ), true);
        $html .= $this->error($model, $field);
        $html .= $this->endControls();

        return $html;
    }

    public function myDropDownList($model, $field, $data = null)
    {
        $fieldName = $field;
        if ($model->isAttributeEnumerable($field)) {
            $data = $model->getAttributeEnums($field);
        } else if ($data === null) {
            $className = $model->getMetaData()->relations[$field]->className;
            $fieldName = $model->getMetaData()->relations[$field]->foreignKey;

            $classModel = call_user_func(array($className, 'model'));

            $data = MyHtml::listData(
                call_user_func(array($classModel, 'findAll')),
                $classModel->getTableSchema()->primaryKey,
                'toString'
            );
        }

        if (!isset($data)) {
            throw new CException(
                'Класс ' . __CLASS__ . 'не нашел данных для аттрибута "' . $field . '" в модели "' . get_class($model) . '"'
            );
        }

        $html = $this->labelEx($model, $fieldName);
        $html .= $this->beginControls();
        $html .= $this->dropDownList(
            $model,
            $fieldName,
            $data,
            array('prompt' => '---')
        );
        $html .= $this->error($model, $fieldName);
        $html .= $this->endControls();

        return $html;
    }

    public function myFileField($model, $field)
    {
        $html = $this->labelEx($model, $field);
        $html .= $this->beginControls();
        $html .= $this->fileField($model, $field);

        if (!$model->isNewRecord && !empty($model->$field) && file_exists($model->uploadTo($field))) {
            $imageLink = MyImage::assetAndCacheImage($model->uploadTo($field), true);
            $imageThumb = MyImage::resizeByWidth($model->uploadTo($field), 150);

            $html .= MyHtml::link(
                $imageThumb, $imageLink,
                array('class' => 'file-field-preview', 'id' => 'file_field_preview_' . $field)
            );
            $this->widget('application.extensions.EColorBox.EColorBox', array(
                'target' => 'a.file-field-preview',
            ));
        }
        $html .= '<br />';
        //        if (!$model->isNewRecord && !empty($model->$field)) {
        //            $html .= '<div id="file_field_remove_button_' . $field . '" style="margin-left: 260px;">';
        //            $html .= MyHtml::button(
        //                'Удалить',
        //                array(
        //                    'onclick' => "javascript:deleteUploadedFile('" . get_class($model) . "', '" . $field . "', '" . $model->primaryKey . "');"
        //                )
        //            );
        //            $html .= '</div>';
        //        }
        $html .= $this->error($model, $field);
        $html .= $this->endControls();

        return $html;
    }

//    public function myAutoCompleteField($model, $field, $sourceUrl) {
//        if (!isset($model->getMetaData()->columns[$field])) {
//            throw new CException('Класс ' . __CLASS__ . ' не нашел аттрибута "'.$field.'" в модели "'.get_class($model).'"');
//        }
//
//        $input_value = $model->$field;
//        if($model->getMetaData()->columns[$field]->isForeignKey) {
//            foreach($model->getMetaData()->relations as $model_relation) {
//                if ($model_relation->foreignKey == $field) {
//                    $input_value = $model->{$model_relation->name};
//                    break;
//                }
//            }
//        }
//
//        $html = $this->labelEx($model, $field);
//        $html .= $this->hiddenField($model, $field);
//        $html .= $this->widget('application.zii.widgets.jui.MyJuiAutoComplete', array(
//            'name' => $field . '_autocomplete',
//            'sourceUrl' => $sourceUrl,
//            'value' => $input_value,
//            'htmlOptions' => array(
//                'size' => 60,
//            ),
//            'options'=>array(
//                'select' =>"js: function(event, ui) {
//                    this.value = ui.item.label;
//                    $(\"#" . get_class($model) . '_' . $field . "\").val(ui.item.id);
//                    if ($.isFunction(" . $field . "_autocomplete_after)) {
//                        " . $field . "_autocomplete_after(ui.item);
//                    }
//                    return false;
//                }",
//            ),
//        ), true);
//        $html .= $this->error($model, $field);
//
//        return $html;
//    }

    public function myGoogleMap($model, $field) {
        $hiddenFieldId = MyHtml::getIdByName(MyHtml::resolveName($model, $field));

        $myClientScript = Yii::app()->getClientScript();
        $myClientScript->registerScriptFile('http://maps.google.com/maps/api/js?sensor=false');
        $myClientScript->registerScript('initializeMap', 'initializeMap();');
        $myClientScript->registerScript('map', '
            var geocoder;
            var map;
            var markersArray = [];
            function initializeMap() {
                geocoder = new google.maps.Geocoder();

                var myLatlng = new google.maps.LatLng(55.755786,37.617633);
                var mapOptions = {
                    zoom: 14,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("map"), mapOptions);

                google.maps.event.addListener(map, \'click\', function(event) {
                    placeMarker(event.latLng);
                });
            }

            function placeMarker(location) {
                $(\'#' . $hiddenFieldId . '\').val(location.lat() + \',\' + location.lng());
                for (var i = 0; i < markersArray.length; i++ ) {
                    markersArray[i].setMap(null);
                }

                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                markersArray.push(marker);
            }

            function findAddress(event) {
                if(event.keyCode==13) {
                    var address = $(\'#mapAddress\').val();
                    geocoder.geocode({\'address\': address}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            map.setCenter(results[0].geometry.location);
//                            var marker = new google.maps.Marker({
//                                map: map,
//                                position: results[0].geometry.location
//                            });
                        } else {
                            alert("Geocode was not successful for the following reason: " + status);
                        }
                    });
                    return false;
                }
                return true;
            }
        ', MyClientScript::POS_END);

        $html = MyHtml::textField('mapAddress', '', array(
            'onkeypress' => 'javascript:return findAddress(event);',
            'placeholder' => 'Поиск по адресу...',
            'class' => 'input-xxlarge search-query',
        ));
        $html .= '<br /><br />';
        $html .= $this->hiddenField($model, $field);
        $html .= '<div id="map" style="height:600px;"></div>';

        return $html;
    }

    public function beginForm()
    {
        //        return '<div class="form">';
    }

    public function endForm()
    {
        //        return '</div>';
    }

    public function beginField()
    {
        return '<div class="control-group">';
    }

    public function endField()
    {
        return '</div>';
    }

    public function beginControls()
    {
        return '<div class="controls">';
    }

    public function endControls()
    {
        return '</div>';
    }

    public function buttons($model)
    {
        $label = $model->isNewRecord ? Yii::t('application', 'Create') : Yii::t('application', 'Save');

        $html = $this->beginButtons($model);
        $html .= '<table><tr><td>';
        $html .= MyHtml::submitButton($label);
        $html .= '</td><td>&nbsp;</td><td>';
        $html .= MyHtml::link('Отмена', array('admin'), array('class' => 'btn'));
        $html .= '</td></tr></table>';
        $html .= $this->endButtons();

        return $html;
    }

    public function beginButtons($model)
    {
        return '<div class="form-actions">';
    }

    public function endButtons()
    {
        return '</div>';
    }

    public function button($model, $label, $submit = false)
    {
        $html = $this->beginButtons($model);
        if ($submit === true) {
            $html .= MyHtml::submitButton($label);
        } else {
            $html .= MyHtml::button($label);
        }
        $html .= $this->endButtons();

        return $html;
    }

    public function labelEx($model, $attribute, $htmlOptions = array())
    {
        $htmlOptions['class'] = 'control-label';
        return parent::labelEx($model, $attribute, $htmlOptions);
    }

    public function errorSummary($models, $header = null, $footer = null, $htmlOptions = array())
    {
        $htmlOptions['class'] = 'alert alert-error';
        return parent::errorSummary($models, $header, $footer, $htmlOptions);
    }
}
