<?php

class MyBackendLinkPager extends CLinkPager
{
    const CSS_HIDDEN_PAGE = 'disabled';
    const CSS_SELECTED_PAGE = 'active';

    public function init()
    {
        $this->cssFile = false;

        $this->prevPageLabel = '&lsaquo;';
        $this->firstPageLabel = '&laquo;';

        $this->nextPageLabel = '&rsaquo;';
        $this->lastPageLabel = '&raquo;';

        $this->header = false;

        parent::init();
    }

    protected function createPageButton($label, $page, $class, $hidden, $selected)
    {
        if ($hidden || $selected) {
            $class .= ' ' . ($hidden ? self::CSS_HIDDEN_PAGE : self::CSS_SELECTED_PAGE);
        }
        return '<li class="' . $class . '">' . CHtml::link($label, $this->createPageUrl($page)) . '</li>';
    }
}
