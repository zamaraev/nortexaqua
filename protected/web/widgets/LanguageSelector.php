<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Александр
 * Date: 20.06.13
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */

class LanguageSelector extends CWidget
{
    public function run()
    {
        $currentLang = Yii::app()->language;
        $languages = Yii::app()->params->languages;
        $this->render('languageSelector', array('currentLang' => $currentLang, 'languages'=>$languages));
    }
}