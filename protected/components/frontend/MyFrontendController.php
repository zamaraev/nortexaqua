<?php

class MyFrontendController extends MyController
{
    protected $title = '';
    protected $description = '';
    protected $keywords = '';
    public $languages;
    protected $system_language;

    const DEFAULT_LANGUAGE = 'ru';

    public function init()
    {
        parent::init();

        $app = Yii::app();
        $this->_detectUserLanguage();

        Yii::app()->getClientScript()->registerCoreScript('jquery');
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function _detectUserLanguage(){

        // If there is a post-request, redirect the application to the provided url of the selected language
        if(isset($_POST['language'])) {
            $lang = $_POST['language'];
            $MultilangReturnUrl = $_POST[$lang];
            $this->redirect($MultilangReturnUrl);

        }
        // Set the application language if provided by GET, session or cookie
        if(isset($_GET['language'])) {
            Yii::app()->language = $_GET['language'];
            Yii::app()->user->setState('language', $_GET['language']);
            $cookie = new CHttpCookie('language', $_GET['language']);
            $cookie->expire = time() + (60*60*24*365); // (1 year)
            Yii::app()->request->cookies['language'] = $cookie;
        }
        else if (Yii::app()->user->hasState('language'))
            Yii::app()->language = Yii::app()->user->getState('language');
        else if(isset(Yii::app()->request->cookies['language']))
            Yii::app()->language = Yii::app()->request->cookies['language']->value;
    }

    public function createMultilanguageReturnUrl($lang=self::DEFAULT_LANGUAGE){
        if (count($_GET)>0){
            $arr = $_GET;
            $arr['language']= $lang;
        }
        else
            $arr = array('language'=>$lang);

        return $this->createUrl('', $arr);
    }

    private function _detectLanguageByClientSystem()
    {
        $system_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        if (isset($_GET['show_system_lang'])) {
            echo $system_lang . "</br>";
        }
        return $system_lang;
    }

    private function _setLanguageState($lang)
    {
        Yii::app()->language = $lang;
        Yii::app()->user->setState('language', $lang);
        $cookie = new CHttpCookie('language', $lang);
        $cookie->expire = time() + 2592000; // Кука на месяц
        Yii::app()->request->cookies['language'] = $cookie;
    }

    private function _detectLanguageByIp()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        //echo "IP: $ip";
        $ripe_content = file_get_contents("http://apps.db.ripe.net/whois/search.json?query-string=$ip&source=ripe");
        $ripe_content = str_replace("\n", '', $ripe_content);
        $ripe_content = str_replace("\t", '', $ripe_content);
        preg_match("/\"name\"\:\"country\"\,\"value\"\:\"(.*)\"/iU", $ripe_content, $country);
        $country = strtolower($country[1]);
        //echo "Страна: $country";
        return $country;
    }

    public function initSeo($model)
    {
        Yii::import('application.modules.seo.models.*');

        if (isset($model->title)) {
            $this->title .= ' ' . $model->title;
        }

        $seoModel = Seo::findByModel($model);
        if ($seoModel !== null) {
            if (!empty($seoModel->title)) {
                $this->title .= ' ' . $seoModel->title;
            }
            if (!empty($seoModel->description)) {
                $this->description .= '. ' . $seoModel->description;
            }
            if (!empty($seoModel->keywords)) {
                $this->keywords .= ',' . $seoModel->keywords;
            }
        }
        return $this;
    }
}
