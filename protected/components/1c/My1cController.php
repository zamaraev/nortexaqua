<?php

class My1cController extends MyController
{
    public function generateId($model, $date, $id)
    {
        switch (get_class($model)) {
            case 'Orders':
                $first = 1;
                break;

            case 'Clients':
                $first = 2;
                break;

            case 'Transactions':
                $first = 3;
                Break;

            default:
                throw new CException();
                break;
        }

        return sprintf('%04d-%s-%012d', $first, date('Ymd', strtotime($date)), $id);
    }

    protected function beforeAction($action)
    {
        return parent::beforeAction($action);
        $allowedActions = array(
            'catalog/checkauth',
            'sale/checkauth',
        );

        $current = sprintf('%s/%s', $action->getController()->id, $action->id);
        if (!in_array($current, $allowedActions)) {
            $cache = Yii::app()->getCache();
            $cookieName = $cache->get('1cCookieName');
            $cookieValue = $cache->get('1cCookieValue');

            $request = Yii::app()->getRequest();

            $actionAllowed = false;
            if ($cookieName != false && $cookieValue != false) {
                $cookies = $request->getCookies();
                if ($cookies->contains($cookieName)) {
                    if ($cookies[$cookieName] == $cookieValue) {
                        $actionAllowed = true;
                    }
                }
            }

            if ($request->getUserHostAddress() == '127.0.0.1') {
                $actionAllowed = true;
            }

            if ($actionAllowed == false) {
                throw new CHttpException(403);
            }
        }

        return parent::beforeAction($action);
    }

    public function date($time = null)
    {
        if ($time == null) {
            $time = time();
        }
        return date('Y-m-d\TH:i:s', $time);
    }
}
