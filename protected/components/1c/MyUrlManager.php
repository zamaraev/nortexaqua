<?php

class MyUrlManager extends CUrlManager
{
    public function parseUrl($request)
    {
        return sprintf('%s/%s', $request->getQuery('type'), $request->getQuery('mode'));
    }
}
