<?php

class Tabs extends CFilter
{
    protected function preFilter($filterChain)
    {
        $controller = $filterChain->controller;

        if (method_exists($controller, 'tabs')) {
            $controller->tabs = call_user_func(array($controller, 'tabs'));
        }

        return true;
    }
}
