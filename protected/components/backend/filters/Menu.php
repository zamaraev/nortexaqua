<?php

class Menu extends CFilter
{
    protected function preFilter($filterChain)
    {
        $controller = $filterChain->controller;
        $actionId = $controller->getAction()->getId();

        $actions = array();
        switch ($actionId) {
            case 'index':
                $actions = array('admin');
            break;

            case 'admin':
                $actions = array('create');
            break;

            case 'create':
                $actions = array('admin');
            break;

            case 'update':
                $actions = array('view', 'admin');
            break;

            case 'view':
                $actions = array('update', 'admin');
            break;
        }

        foreach($actions as $action) {
            $menuAction = $this->_menuAction($controller, $action);
            if ($menuAction !== false) {
                $controller->menu[] = $menuAction;
            }
        }

        return true;
    }

    private function _menuAction($controller, $action) {
        $urlParts = array();
        if (isset($controller->model)) {
            $controllerQuery = Yii::app()->getRequest()->getQuery($controller->model);
            if ($controllerQuery !== null) {
                foreach ($controllerQuery as $key => $value) {
                    $urlParts[$controller->model][$key] = $value;
                }
            }
        }

        $menuAction = false;
        if (key_exists($action, $controller->actions()) || method_exists($controller, 'action' . ucfirst($action))) {
            $menuAction = array(
                'label' => $this->_icon($action) . Yii::t('application', ucfirst($action)),
                'url' => CMap::mergeArray(array($action), $urlParts),
            );
        }

        return $menuAction;
    }

    private function _icon($action) {
        $iconClass = false;

        switch ($action) {
            case 'update':
                $iconClass = 'icon-pencil';
            break;

            case 'admin':
                $iconClass = 'icon-th-list';
            break;

            case 'create':
                $iconClass = 'icon-plus-sign';
            break;
        }

        $ret = '';
        if ($iconClass !== false) {
            $ret = '<i class="' . $iconClass . '"></i> ';
        }

        return $ret;
    }
}
