<?php

class Breadcrumbs extends CFilter
{
    protected function preFilter($filterChain)
    {
        $controller = $filterChain->controller;
        $actionId = $controller->getAction()->getId();
        if (isset($controller->model)) {
            $modelHumanId = trim(preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $controller->model));

            $modelHumanUrlParts = array();
            $controllerQuery = Yii::app()->getRequest()->getQuery($controller->model);
            if ($controllerQuery !== null) {
                foreach ($controllerQuery as $key => $value) {
                    $modelHumanUrlParts[$controller->model][$key] = $value;
                }
            }

            if (method_exists($controller, 'breadcrumbModelHumanId')) {
                $_modelHumanId = $controller->breadcrumbModelHumanId($controllerQuery);
                if ($_modelHumanId) {
                    $modelHumanId = $_modelHumanId;
                }
            }

            $translatePath = MyActiveRecord::getTranslatePath(preg_replace('/\s/', '', $modelHumanId));
            $modelHumanTranslated = Yii::t($translatePath, 'toString');
            if ($modelHumanTranslated == 'toString') {
                $modelHumanTranslated = $modelHumanId;
            }

            $modelHuman = array($modelHumanTranslated => CMap::mergeArray(
                array('admin'),
                $modelHumanUrlParts
            ));

            switch ($actionId) {
                case 'admin':
                    $controller->breadcrumbs = CMap::mergeArray(
                        $modelHuman,
                        array(Yii::t('application', 'Admin'))
                    );
                    break;

                case 'create':
                    $controller->breadcrumbs = CMap::mergeArray(
                        $modelHuman,
                        array(Yii::t('application', 'Create'))
                    );
                    break;

                case 'update':
                    $controller->breadcrumbs = CMap::mergeArray(
                        $modelHuman,
                        array(Yii::t('application', 'Update'))
                    );
                    break;

                case 'view':
                    $controller->breadcrumbs = CMap::mergeArray(
                        $modelHuman,
                        array(Yii::t('application', 'View'))
                    );
                    break;
            }

            if ($controller->breadcrumbs !== null && method_exists($controller, 'breadcrumbs')) {
                $controller->breadcrumbs = CMap::mergeArray(
                    $controller->breadcrumbs(),
                    $controller->breadcrumbs
                );
            }
        }

        return true;
    }
}
