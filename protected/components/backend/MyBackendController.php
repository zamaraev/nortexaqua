<?php

class MyBackendController extends MyController
{
    public $breadcrumbs;
    public $menu;
    public $tabs;

    public function filters()
    {
        return array(
            'accessControl',
            array('application.components.backend.filters.PageTitle - login, delete'),
            array('application.components.backend.filters.Breadcrumbs - login, delete'),
            array('application.components.backend.filters.Menu - login, delete'),
            array('application.components.backend.filters.Tabs'),
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'users' => array('*'),
                'actions' => array('login'),
            ),
            array(
                'allow',
                'users' => array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }
}
