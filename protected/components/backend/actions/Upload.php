<?php

Yii::import('application.extensions.EFileUploadAction.EFileUploadAction');

class Upload extends EFileUploadAction
{
    public function run()
    {
        $controller = $this->getController();

        $this->name = 'file';
        $this->path = Yii::getPathOfAlias('webroot') . '/uploads/' . $controller->getModule()->id . '/' . date('Y-m-d');
        $this->createDirectory = true;
        $this->createDirectoryMode = 0755;
        $this->createDirectoryRecursive = true;
        $this->filenameRule = 'md5($file->name) . "." . $file->extensionName';
        $this->onAfterSave = array($this, 'onAfterSave');

        parent::run();
    }

    public function onAfterSave($event)
    {
        $path = Yii::app()->getRequest()->baseUrl . '/uploads/' . Yii::app()->getController()->getModule()->id . '/' . date('Y-m-d');
        $filename = $event->sender->filename;
        printf('%s/%s', $path, $filename);
        exit;
    }
}
