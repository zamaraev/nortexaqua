<?php

class Sort extends CAction
{
    public $attribute;
    public $begin = 1;

    public function run()
    {
        $controller = $this->getController();

        if (count($_POST) === 1) {
            $key = key($_POST);
            $sorts = Yii::app()->getRequest()->getPost($key);
            if ($sorts !== null) {
                foreach ($sorts as $id) {
                    $model = $controller->loadModel($id);
                    $model->{$this->attribute} = $this->begin++;
                    $model->save();
                }
            }
        }

        Yii::app()->end();
    }
}
