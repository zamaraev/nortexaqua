<?php

class RedirectToAdmin extends CAction
{
    public function run()
    {
        $this->getController()->redirect(array('admin'));
    }
}
