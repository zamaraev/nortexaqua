<?php

class Admin extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $model = new $controller->model('search');
        $model->unsetAttributes();

        $params = Yii::app()->getRequest()->getQuery($controller->model, null);
        if ($params !== null) {
            $model->attributes = $params;
        }

        $renderVars = array('model' => $model);
        if (method_exists($controller, 'onAdminBeforeRender')) {
            $fromAdminRenderVars = $controller->onAdminBeforeRender(get_defined_vars());
            if (!empty($fromAdminRenderVars)) {
                $renderVars = array_merge($renderVars, $fromAdminRenderVars);
            }
        }
        $renderViewFile = 'admin';
        if ($controller->getViewFile($renderViewFile) === false) {
            $renderViewFile = 'application.components.backend.actions.views.' . $renderViewFile;
        }

        $controller->render($renderViewFile, $renderVars);
    }
}
