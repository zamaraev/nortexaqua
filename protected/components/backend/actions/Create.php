<?php

class Create extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $model = new $controller->model('create');

        if (isset($_GET[$controller->model])) {
            $model->attributes = $_GET[$controller->model];
        }

        if (isset($_POST[$controller->model])) {
            $model->attributes = $_POST[$controller->model];

            if (method_exists($controller, 'onCreateAfterAttributesSet')) {
                $controller->onCreateAfterAttributesSet(get_defined_vars());
            }

            $controller->performUploads($model);
            if ($model->save()) {
                $controller->setFlash('default', 'Запись успешно добавлена');
                $controller->performUploadsSaveToDisk($model);

                if (method_exists($controller, 'onCreateAfterSave')) {
                    $controller->onCreateAfterSave(get_defined_vars());
                } else {
                    $this->afterSave();
                }
            }
        }

        if (method_exists($controller, 'onCreateBeforeRender')) {
            $controller->onCreateBeforeRender(get_defined_vars());
        }

        $renderViewFile = 'create';
        if ($controller->getViewFile($renderViewFile) === false) {
            $renderViewFile = 'application.components.backend.actions.views.' . $renderViewFile;
        }

        $renderVars = array('model' => $model);
        $controller->render($renderViewFile, $renderVars);
    }

    protected function afterSave() {
        $controller = $this->getController();

        if (isset($controller->model)) {
            $urlParts = array();
            $controllerQuery = Yii::app()->getRequest()->getQuery($controller->model);
            if ($controllerQuery !== null) {
                foreach($controllerQuery as $key=>$value) {
                    $urlParts[$controller->model][$key] = $value;
                }
            }

            $this->getController()->redirect(CMap::mergeArray(array('admin'), $urlParts));
        } else {
            $this->getController()->redirect(array('admin'));
        }
    }
}
