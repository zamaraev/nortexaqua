<?php

$gridColumns = array();

$adminAttributesToShow = array();
if (method_exists($this, 'adminAttributes')) {
    $adminAttributesToShow = $this->adminAttributes();
    foreach ($adminAttributesToShow as $attribute) {
        if (isset($attribute[0]) && count($attribute) == 1) {
            $gridColumns[] = $attribute[0];
        } else {
            $gridColumns[] = $attribute;
        }
    }
}

$gridColumns[] = array('class' => 'MyButtonColumn');

$config = array(
    'dataProvider' => $model->search(),
    'columns' => $gridColumns,
);

if (method_exists($this, 'adminConfig')) {
    $adminAdditionalConfig = $this->adminConfig();
    foreach ($adminAdditionalConfig as $key => $value) {
        $config[$key] = $value;
    }
}

$this->widget('application.zii.widgets.grid.MyGridView', $config);
