<?php

$renderViewFile = 'application.modules.' . Yii::app()->getController()->getModule()->id . '.views.backend.' . Yii::app()->getController()->id . '._form';
if ($this->getViewFile($renderViewFile) === false) {
    $renderViewFile = 'application.components.backend.actions.views._form';
}

echo $this->renderPartial($renderViewFile, array('model' => $model));
