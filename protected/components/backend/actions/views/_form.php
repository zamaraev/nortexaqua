<?php

/** @var $form MyBackendActiveForm */
$form = $this->beginWidget('MyBackendActiveForm', array(
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    )
));

echo $form->beginForm();
echo $form->errorSummary($model);

$modelColumns = $model->getMetaData()->columns;

$formAttributesToShow = array();
if (method_exists($this, 'formAttributes')) {
    $formAttributesToShow = $this->formAttributes();
} else {
    foreach ($modelColumns as $modelColumn) {
        $formAttributesToShow[] = array($modelColumn->name);
    }
}

foreach ($formAttributesToShow as $attribute) {
    if (!isset($attribute[1])) {
        echo $form->beginField();
        echo $form->renderField($model, $attribute[0]);
        echo $form->endField();
    } else {
        switch ($attribute[1]) {
            case 'beginfieldset':
                echo '<fieldset><legend>'. $attribute[0] . '</legend>';
                break;

            case 'endfieldset':
                echo '</fieldset>';
                break;

            case 'html':
                echo $form->beginField();
                echo $form->myTextEditorField($model, $attribute[0]);
                echo $form->endField();
                break;

            case 'phone':
                echo $form->beginField();
                echo $form->myPhoneField($model, $attribute[0]);
                echo $form->endField();
                break;

            case 'gallery':
                echo $form->beginField();
                $this->widget('application.extensions.EGallery.EGallery', array(
                    'model' => $model,
                    'attribute' => 'pagePictures',
                ));
                echo $form->endField();
                break;

            case 'json':
                $json = $model->$attribute[0];
                if ($json) {
                    foreach($json as $k=>$v) {
                        echo $form->beginField();
                        echo $form->myTextField($model, $attribute[0] . '[' . $k . ']');
                        echo $form->endField();
                    }
                }
                break;

            case 'text':
                $htmlOptions = array();
                if ($attribute[2]) {
                    $htmlOptions = $attribute[2];
                }
                echo $form->beginField();
                echo $form->myTextField($model, $attribute[0], $htmlOptions);
                echo $form->endField();
                break;

            default:
                throw new CException();
                break;
        }
    }
}

$renderViewFile = 'application.modules.' . Yii::app()->getController()->getModule()->id . '.views.backend.' . Yii::app()->getController()->id . '._formBeforeEnd';
if ($this->getViewFile($renderViewFile)) {
    $this->renderPartial($renderViewFile, array('model' => $model, 'form' => $form));
}

echo $form->buttons($model);
echo $form->endForm();

$this->endWidget();
