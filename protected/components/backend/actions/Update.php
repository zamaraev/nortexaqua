<?php

class Update extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $params = $controller->getActionParams();
        $model = $controller->loadModel($params['id']);
        $model->setScenario('update');

        if (isset($_POST[$controller->model])) {
            //var_dump($_POST[$controller->model]);die;
            $model->attributes = $_POST[$controller->model];
            $controller->performUploads($model);
            if ($model->save()) {
                $controller->setFlash('default', 'Запись успешно обновлена');
                $controller->performUploadsSaveToDisk($model);

                if (method_exists($controller, 'onUpdateAfterSave')) {
                    $controller->onUpdateAfterSave(get_defined_vars());
                } else {
                    if (method_exists($controller, 'onUpdateAfterSaveRedirectTo')) {
                        $redirectTo = $controller->onUpdateAfterSaveRedirectTo(get_defined_vars());
                    } else {
                        $redirectTo = array('admin');
                    }
                    $controller->redirect($redirectTo);
                }
            }
        }

        $renderViewFile = 'update';
        if ($controller->getViewFile($renderViewFile) === false) {
            $renderViewFile = 'application.components.backend.actions.views.' . $renderViewFile;
        }

        $renderVars = array('model' => $model);
        if (method_exists($controller, 'onUpdateBeforeRender')) {
            $fromAdminRenderVars = $controller->onUpdateBeforeRender(get_defined_vars());
            if (!empty($fromAdminRenderVars)) {
                $renderVars = array_merge($renderVars, $fromAdminRenderVars);
            }
        }
        $controller->render($renderViewFile, $renderVars);
    }
}
