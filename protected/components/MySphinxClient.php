<?php

class MySphinxClient extends CApplicationComponent
{
    /**
     * @var SphinxClient
     */
    private $_cl;
    private $_src;

    public function __construct($src)
    {
        require dirname(__FILE__) . '/../../libs/vendor/sphinx/sphinxapi.php';

        $this->_src = $src;

        $this->_cl = new SphinxClient;
        $this->_cl->SetServer('localhost', 9312);
        $this->_cl->SetConnectTimeout(1);
        $this->_cl->SetArrayResult(true);
        $this->_cl->SetMatchMode(SPH_MATCH_EXTENDED2);
        $this->_cl->SetSelect('*');
    }

    public function query($q, $partial=true)
    {
        if ($partial == true) {
            $str = trim($q, '*') . '*';
        } else {
            $str = $q;
        }
        $res = $this->_cl->Query($str, $this->_src);

        if ($this->_cl->GetLastError()) {
            throw new CException($this->_cl->GetLastError());
        }

        return $res;
    }
}
