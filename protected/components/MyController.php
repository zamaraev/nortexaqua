<?php

class MyController extends CController
{
    public function init()
    {
        $this->layout = '//layouts/main';
        parent::init();
    }

    public function setFlash($key, $value)
    {
        Yii::app()->user->setFlash($key, $value);
    }

    public function getFlash($key)
    {
        return Yii::app()->user->getFlash($key);
    }

    public function hasFlash($key)
    {
        return Yii::app()->user->hasFlash($key);
    }

    public function loadModel($id)
    {
        $model = MyActiveRecord::model($this->model)->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    public function performUploads($model)
    {
        if (!empty($_FILES)) {
            foreach ($_FILES[$this->model]['name'] as $_attribute => $_file) {
                if (!empty($_file)) {
                    $model->$_attribute = CUploadedFile::getInstance($model, $_attribute);
                }
            }
        }
    }

    public function performUploadsSaveToDisk($model)
    {
        if (!empty($_FILES)) {
            foreach ($_FILES[$this->model]['name'] as $_attribute => $_file) {
                if (!empty($_file)) {
                    $model->$_attribute->saveAs(MyFilesystem::makeDirs(Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $model->uploadTo($_attribute)));
                }
            }
        }
    }

    public function getQuery($name, $defaultValue = null)
    {
        return Yii::app()->getRequest()->getQuery($name, $defaultValue);
    }

    public function getPost($name, $defaultValue = null)
    {
        return Yii::app()->getRequest()->getPost($name, $defaultValue);
    }


}
