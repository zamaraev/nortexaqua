<?php
/**
 * <code>
 * MyMailer::getInstance()
 *        ->setTo()
 *        ->setSubject('subject')
 *        ->setText('text')
 *        ->send();
 * </code>
 */
class MyMailer extends CApplicationComponent
{
    public $subject;
    public $to;
    public $cc;
    public $view;
    public $data;
    public $text;
    private $_contentType = 'text/plain';

    public static function getInstance()
    {
        return new self();
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    public function setCc($cc)
    {
        $this->cc = $cc;
        return $this;
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function setView($view)
    {
        $this->view = $view;
        return $this;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function setContentType($contentType)
    {
        $this->_contentType = $contentType;

        return $this;
    }

    public function send()
    {
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        include(dirname(__FILE__) . '/../../libs/vendor/swift/swift_required.php');
        spl_autoload_register(array('YiiBase', 'autoload'));

        $message = Swift_Message::newInstance()
            ->setSubject($this->subject)
            ->setFrom(Yii::app()->params['adminEmail'])
            ->setTo($this->to)
            ->setCc($this->cc);

        if ($this->view) {
            $message->setBody(self::renderView($this->view, $this->data), $this->_contentType, 'utf-8');
        } else {
            $message->setBody($this->text, $this->_contentType, 'utf-8');
        }

        $transport = Swift_SmtpTransport::newInstance('smtp.yandex.ru')
            ->setUsername('send@nortexaqua.ru')
            ->setPassword('admin_123123');

        $mailer = Swift_Mailer::newInstance($transport);
        return $mailer->send($message);
    }

    public static function renderView($view, $data)
    {
        $controller = Yii::app()->getController();
        $view_file = $controller->getViewFile('/mailer/' . $view);

        return $controller->renderFile($view_file, $data, true);
    }
}
