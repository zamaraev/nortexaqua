<?php

class MyDbMigration extends EDbMigration
{
    public $autoColumns;
    public $autoTables;
    public $autoForeignKeys;

    public function getIsPostgreSQL()
    {
        return strstr(Yii::app()->db->connectionString, 'pgsql:');
    }

    public function getIsMySQL()
    {
        return strstr(Yii::app()->db->connectionString, 'mysql:');
    }

    public function sequence($table, $increment)
    {
        if ($this->getIsPostgreSQL()) {
            $this->execute('ALTER SEQUENCE "' . $table . '_id_seq" RESTART ' . $increment . ';');
        } else if ($this->getIsMySQL()) {
            $this->execute('ALTER TABLE `' . $table . '` AUTO_INCREMENT = ' . $increment . ';');
        } else {
            throw new CException();
        }
    }

    public function autoTable($table, $columns, $options = null)
    {
        $this->autoTables[] = array($table, $columns, $options);
    }

    public function autoColumn($table, $column, $type)
    {
        $this->autoColumns[] = array($table, $column, $type);
    }

    public function autoForeignKey($table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        $this->autoForeignKeys[] = array($table . '_' . $columns . '_fkey', $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    public function safeUp()
    {
        if (isset($this->autoTables)) {
            foreach ($this->autoTables as $table) {
                call_user_func_array(array($this, 'createTable'), $table);
            }
        }

        if (isset($this->autoColumns)) {
            foreach ($this->autoColumns as $column) {
                call_user_func_array(array($this, 'addColumn'), $column);
            }
        }

        if (isset($this->autoForeignKeys)) {
            foreach ($this->autoForeignKeys as $foreignKey) {
                call_user_func_array(array($this, 'addForeignKey'), $foreignKey);
            }
        }

        parent::safeUp();
    }

    public function safeDown()
    {
        if (isset($this->autoForeignKeys)) {
            foreach ($this->autoForeignKeys as $foreignKey) {
                call_user_func_array(array($this, 'dropForeignKey'), array($foreignKey[0], $foreignKey[1]));
            }
        }

        if (isset($this->autoColumns)) {
            foreach ($this->autoColumns as $column) {
                call_user_func_array(array($this, 'dropColumn'), $column);
            }
        }

        if (isset($this->autoTables)) {
            foreach ($this->autoTables as $table) {
                call_user_func_array(array($this, 'dropTable'), array($table[0]));
            }
        }

        parent::safeDown();
    }
}
