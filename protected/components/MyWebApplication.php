<?php

class MyWebApplication extends CWebApplication
{
    /**
     * @return MyClientScript
     */
    public function getClientScript()
    {
        return parent::getClientScript();
    }

    /**
     * @return MyFrontendWebUser|MyBackendWebUser
     */
    public function getUser()
    {
        return parent::getUser();
    }
}
