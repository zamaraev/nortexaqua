<?php

class MyJsonableAttributeBehavior extends CActiveRecordBehavior
{
    public $attribute;
    public $default;

    public function afterConstruct($event)
    {
        $owner = $this->getOwner();
        $attribute = $this->attribute;

        if (!$owner->$attribute) {
            $owner->$attribute = $this->default;
        }

        parent::afterConstruct($event);
    }

    public function afterFind($event)
    {
        $owner = $this->getOwner();
        $attribute = $this->attribute;

        $owner->$attribute = json_decode(trim(str_replace('\\', '', $owner->$attribute), '"'), true);

        if (!$owner->$attribute || !is_array($owner->$attribute)) {
            if (!$this->default) {
                $this->default = array();
            }
            $owner->$attribute = $this->default;
        }

        parent::afterFind($event);
    }

    public function beforeValidate($event)
    {
        $owner = $this->getOwner();
        $attribute = $this->attribute;

        $owner->$attribute = json_encode($owner->$attribute);

        parent::beforeValidate($event);
    }

    public function beforeSave($event)
    {
        $owner = $this->getOwner();
        $attribute = $this->attribute;

        $owner->$attribute = json_encode($owner->$attribute);

        parent::beforeSave($event);
    }

    public function afterValidate($event)
    {
        $owner = $this->getOwner();

        $attribute = $this->attribute;

        $owner->$attribute = json_decode(trim(str_replace('\\', '', $owner->$attribute), '"'), true);

        if (!$owner->$attribute || !is_array($owner->$attribute)) {
            if (!$this->default) {
                $this->default = array();
            }
            $owner->$attribute = $this->default;
        }

        parent::afterValidate($event);
    }
}
