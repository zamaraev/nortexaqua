<?php

class MyActiveRecord extends CActiveRecord
{
    /**
     * @param $pk
     * @param string $condition
     * @param array $params
     * @return self
     */
    public function findByPk($pk, $condition = '', $params = array())
    {
        return parent::findByPk($pk, $condition, $params);
    }

    /**
     * @param $attributes
     * @param string $condition
     * @param array $params
     * @return self
     */
    public function findByAttributes($attributes, $condition = '', $params = array())
    {
        return parent::findByAttributes($attributes, $condition, $params);
    }

    public function __toString()
    {
        return sprintf('Объект %s', get_class($this));
    }

    public function getToString()
    {
        return $this->__toString();
    }

    public function tableName()
    {
        return preg_replace('/_base$/', '', trim(strtolower(preg_replace('/(?<![A-Z])[A-Z]/', '_\0', get_class($this))), '_'));
    }

    public function attributeLabels()
    {
        $t = array();

        $modelReflection = new ReflectionClass($this);
        $modelPropertiesDirty = $modelReflection->getProperties();
        $modelPropertiesClean = array();
        foreach ($modelPropertiesDirty as $modelProperty) {
            if ($modelProperty->class == get_class($this)) {
                $modelPropertiesClean[] = $modelProperty->name;
            }
        }
        $modelJsonableAttributes = array();
        foreach ($this->behaviors() as $behavior) {
            if (is_array($behavior) && isset($behavior['class']) && $behavior['class'] == 'MyJsonableAttributeBehavior') {
                if (is_array($this->$behavior['attribute'])) {
                    foreach (array_keys($this->$behavior['attribute']) as $key) {
                        $modelJsonableAttributes[] = $behavior['attribute'] . '[' . $key . ']';
                    }
                }
            }
        }

        $attributes = array_merge(
            array_keys($this->getMetaData()->columns),
            array_keys($this->getMetaData()->relations),
            $modelPropertiesClean,
            $modelJsonableAttributes
        );

        foreach ($attributes as $attribute) {
            $t[$attribute] = self::attributeLabel($this, $attribute);
        }

        return $t;
    }

    public static function attributeLabel($modelClass, $attribute)
    {
        $attributeName = self::attributeHumanName($modelClass, $attribute);

        $attributeTranslated = Yii::t(self::getTranslatePath($modelClass), $attributeName);
        $defaultTranslation = Yii::t('models', $attributeName);

        if ($attributeTranslated !== $attributeName) {
            $t = $attributeTranslated;
        } else {
            $t = $defaultTranslation;
        }

        return $t;
    }

    public static function attributeHumanName($modelClass, $attribute)
    {
        if ($modelClass instanceof MyActiveRecord) {
            $md = $modelClass->getMetaData();
            foreach ($md->relations as $relation) {
                if ($relation->foreignKey == $attribute) {
                    $attribute = preg_replace('/_id$/', '', strtolower(preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute)));
                    break;
                }
            }
        }

        $attribute = preg_replace('/[A-Z]/', ' \0', $attribute);
        $attribute = str_replace('_', ' ', $attribute);
        $attribute = ucfirst(strtolower($attribute));

        return $attribute;
    }

    public static function getTranslatePath($modelClass)
    {
        if (is_object($modelClass)) {
            $modelClassName = get_class($modelClass);
        } else {
            $modelClassName = $modelClass;
        }

        $inModule = !Yii::app() instanceof CConsoleApplication;
        $inModule = $inModule && Yii::app()->getController() && Yii::app()->getController()->getModule();
        $inModule = $inModule && Yii::app()->getController()->getModule()->getId();

        if ($inModule === false) {
            $translatePath = 'models/' . $modelClassName;
        } else {
            $moduleName = Yii::app()->getController()->getModule()->getId();
            $translatePath = $moduleName . 'Module.models/' . $modelClassName;
        }

        return $translatePath;
    }

    /**
    public function findOrInitializeRelation($name)
    {
    $relation = $this->$name;

    if ($relation == null) {
    $class_name = $this->getActiveRelation($name)->className;
    $foreign_key = $this->getActiveRelation($name)->foreignKey;

    $model = new $class_name;
    $model->$foreign_key = $this->id;
    return $model;
    } else {
    return $relation;
    }
    }
     */

    public function findOrCreateByAttributes($attributes)
    {
        if ($model = $this->findByAttributes($attributes)) {
            return $model;
        } else {
            $model = new $this;
            $model->attributes = $attributes;
            $model->save();
            return $model;
        }
    }

     public function findOrCreateByAttributesAndReturnPk($attributes)
    {
        return $this->findOrCreateByAttributes($attributes)->getPrimaryKey();
    }

    /**
     * Например, форматирует "2012-02-07 14:31:01.000429" в "07.02.2012 14:31:01" для нормального отображения.
     *
     * @return void
     */
    public function afterFind()
    {
        foreach ($this->getMetaData()->columns as $column) {
            $column_name = $column->name;

            switch ($column->dbType) {
                case 'date':
                    $this->setAttribute(
                        $column_name,
                        empty($this->$column_name) ? null : date('d.m.Y', strtotime($this->$column_name))
                    );
                    break;

                case 'timestamp without time zone':
                    $this->setAttribute(
                        $column_name,
                        empty($this->$column_name) ? null : date('d.m.Y H:i:s', strtotime($this->$column_name))
                    );
                    break;
            }
        }
        parent::afterFind();
    }

    protected function beforeValidate()
    {
        if ($this->isNewRecord && $this->hasAttribute('created_at')) {
            $this->created_at = date('c');
        }

        return parent::beforeValidate();
    }

    public function beforeSave()
    {
        if (!$this->isNewRecord && $this->hasAttribute('updated_at')) {
            $this->updated_at = date('c');
        }

        /**
         * Например, форматирует "07.02.2012 14:31:01" в "2012-02-07 14:31:01.000429" для нормального сохранения.
         */
        foreach ($this->getMetaData()->columns as $column) {
            $columnName = $column->name;

            switch ($column->dbType) {
                case 'date':
                case 'timestamp without time zone':
                    $this->setAttribute(
                        $columnName,
                        empty($this->$columnName) ? null : date('c', strtotime($this->$columnName))
                    );
                    break;

                case 'integer':
                case 'bigint':
                    if (empty($this->$columnName)) {
                        $this->setAttribute($columnName, null);
                    }
                    break;
            }
        }

        return parent::beforeSave();
    }

    public function isAttributeEnumerable($attribute)
    {
        if (!isset($this->getMetaData()->relations[$attribute])) {
            $modelReflection = new ReflectionClass($this);
            foreach (array_keys($modelReflection->getConstants()) as $constant) {
                if (preg_match('/^' . strtoupper($attribute) . '.+/', $constant)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getAttributeEnums($attribute)
    {
        $return = array();

        $modelReflection = new ReflectionClass($this);
        foreach ($modelReflection->getConstants() as $constant => $value) {
            if (preg_match('/^' . strtoupper($attribute) . '.+/', $constant)) {
                $return[$value] = Yii::t(self::getTranslatePath($this), $constant);
            }
        }

        return $return;
    }

    public function getAttributeEnum($attribute)
    {
        $enums = $this->getAttributeEnums($attribute);
        if (!empty($this->$attribute)) {
            return $enums[$this->$attribute];
        }
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        foreach ($this->getMetaData()->columns as $name => $column) {
            if ($column->type === 'string') {
                $criteria->compare($name, $this->$name, true);
            } else {
                $criteria->compare($name, $this->$name);
            }
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function uploadTo($attribute)
    {
        switch ($attribute) {
            default:
                return 'uploads' . DIRECTORY_SEPARATOR . get_class($this) . DIRECTORY_SEPARATOR . $this->id . DIRECTORY_SEPARATOR . $this->$attribute;
                break;
        }
    }

    public function deleteRecursive($relations = array())
    {
        foreach($relations as $relation) 
        {
            if(is_array($this->$relation))
            {
                foreach($this->$relation as $relation_item)
                {
                    $relation_item->deleteRecursive();
                }
            } 
            else 
            {
                $this->$relation->deleteRecursive();
            }
        }
        $this->delete();
    }
}
