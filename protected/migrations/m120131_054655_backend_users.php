<?php

class m120131_054655_backend_users extends EDbMigration
{
    private $_table = 'backend_users';

    public function safeUp()
    {
        $this->createTable($this->_table, array(
            'id' => 'pk',
            'login' => 'varchar(20) NOT NULL',
            'password' => 'varchar(40) NOT NULL',
            'status' => 'boolean NOT NULL DEFAULT true',
            'created_at' => 'timestamp NOT NULL DEFAULT NOW()',
            'updated_at' => 'timestamp',
        ), 'ENGINE InnoDB');

        $this->insert($this->_table, array(
            'login' => 'admin',
            'password' => sha1('123123123'),
            'created_at' => date('c'),
        ));
    }

    public function safeDown()
    {
        $this->dropTable($this->_table);
    }
}
