<?php

class m140428_164351_create_blog extends MyDbMigration
{
    public function safeUp()
    {
        $this->createTable('blog_posts', array(
            'id' => 'pk',
            'title_lang1' => 'string',
            'title_lang2' => 'string',

            'text_lang1' => 'text',
            'text_lang2' => 'text',

            'text_for_twitter_lang1' => 'text', // Русский текст для твиттера
            'text_for_twitter_lang2' => 'text', // Англ текст для твиттера
            'tags' => 'string',
            'date' => 'date',

        ));
    }

    public function safeDown()
    {
        $this->dropTable('blog_posts');
    }
}
