<?php

class m120131_162303_run_dump_from_cdb_auth_manager extends EDbMigration
{
    private $sql;

    public function safeUp()
    {
        $this->sql[] = 'drop table if exists auth_item;';
        $this->sql[] = 'drop table if exists auth_item_child;';
        $this->sql[] = 'drop table if exists auth_assignment;';

        $this->sql[] = 'create table auth_item
        (
           name                 varchar(64) not null,
           type                 integer not null,
           description          text,
           bizrule              text,
           data                 text,
           primary key (name)
        );';

        $this->sql[] = 'create table auth_item_child
        (
           parent               varchar(64) not null,
           child                varchar(64) not null,
           primary key (parent,child),
           foreign key (parent) references auth_item (name) on delete cascade on update cascade,
           foreign key (child) references auth_item (name) on delete cascade on update cascade
        );';

        $this->sql[] = 'create table auth_assignment
        (
           itemname             varchar(64) not null,
           userid               varchar(64) not null,
           bizrule              text,
           data                 text,
           primary key (itemname,userid),
           foreign key (itemname) references auth_item (name) on delete cascade on update cascade
        );';

        foreach ($this->sql as $sql) {
            $this->execute($sql);
        }
    }

    public function safeDown()
    {
        $this->execute('drop table if exists auth_item_child');
        $this->execute('drop table if exists auth_assignment');
        $this->execute('drop table if exists auth_item');
    }
}
