<?php

class m120131_162435_add_default_roles extends EDbMigration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO auth_item VALUES ('admin', '2', '', '', null);");
        $this->execute("INSERT INTO auth_assignment VALUES ('admin', 'admin', null, null);");
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM auth_assignment;");
        $this->execute("DELETE FROM auth_item;");
    }
}
