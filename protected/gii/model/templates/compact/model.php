<?php
/**
 * This is the template for generating the model class of a specified table.
 * - $this: the ModelCode object
 * - $tableName: the table name for this class (prefix is already removed if necessary)
 * - $modelClass: the model class name
 * - $columns: list of table columns (name=>CDbColumnSchema)
 * - $labels: list of attribute labels (name=>label)
 * - $rules: list of validation rules
 * - $relations: list of relations (name=>relation declaration)
 */

if (strpos($this->modelPath, 'modules') !== false) {
    $moduleName = ucfirst(str_replace(array('application.modules.', '.models'), '', $this->modelPath)) . 'Module';
} else {
    $moduleName = '';
}
?>
<?php echo "<?php\n"; ?>

/**
 * Класс модели для таблицы "<?php echo $tableName; ?>".
 *
<?php foreach($columns as $column): ?>
 * @property <?php echo $column->type.' $'.$column->name."\n"; ?>
<?php endforeach; ?>
<?php if(!empty($relations)): ?>
 *
 * Модель имеет следующие связи:
<?php foreach($relations as $name=>$relation): ?>
 * @property <?php
    if (preg_match("~^array\(self::([^,]+), '([^']+)', '([^']+)'\)$~", $relation, $matches))
    {
        $relationType = $matches[1];
        $relationModel = preg_replace('/Base$/', '', $matches[2]);

        switch($relationType){
            case 'HAS_ONE':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'BELONGS_TO':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'HAS_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            case 'MANY_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            default:
                echo 'mixed $'.$name."\n";
        }
    }
    ?>
<?php endforeach; ?>
<?php endif; ?>
 *
 * @package Models
 * @author  Dmitriy Neshin <just.neshin@gmail.com>
 */
class <?php echo $modelClass; ?> extends <?php echo $this->baseClass . "\n"; ?>
{
    /**
     * Правила.
     *
     * @return array
     */
    public function rules()
    {
        return array(
<?php foreach($rules as $rule): ?>
            <?php echo str_replace("=>", " => ", $rule).",\n"; ?>
<?php endforeach; ?>
            array('<?php echo implode(', ', array_keys($columns)); ?>', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Связи.
     *
     * @return array
     */
    public function relations()
    {
        return array(
<?php foreach($relations as $name=>$relation): ?>
            <?php echo "'$name' => ".preg_replace('/Base/', '', $relation).",\n"; ?>
<?php endforeach; ?>
        );
    }
}
