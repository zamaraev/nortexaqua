<?php echo "<?php\n"; ?>

class DefaultController extends MyBackendController
{
    public $model = 'Suppliers';

    public function actions()
    {
        return array(
            'index' => 'application.components.backend.actions.RedirectToAdmin',
            'admin' => 'application.components.backend.actions.Admin',
            'update' => 'application.components.backend.actions.Update',
            'create' => 'application.components.backend.actions.Create',
        );
    }
}
