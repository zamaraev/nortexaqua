<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n\n\$form = \$this->beginWidget('MyBackendActiveForm');\n"; ?>

<?php echo "echo \$form->beginForm();\necho \$form->errorSummary(\$model);"; ?>

<?php
foreach($this->tableSchema->columns as $column)
{
    if($column->isPrimaryKey)
        continue;
    echo "\necho \$form->beginField();\necho \$form->renderField(\$model, '{$column->name}');\necho \$form->endField();\n";
}

echo "\necho \$form->buttons(\$model);\necho \$form->endForm();\n";
echo "\n\$this->endWidget();\n";
