<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php
echo "<?php ";
$label=$this->pluralize($this->class2name($this->modelClass));
?>
<?php /*echo "<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>";*/ ?>
<?php
/*
<div class="search-form" style="display:none">
<?php echo "<?php \$this->renderPartial('_search',array(
    'model'=>\$model,
)); ?>\n"; ?>
</div><!-- search-form -->
*/
?>$this->widget('application.zii.widgets.grid.MyGridView', array(
    'dataProvider' => $model->search(),
    'columns' => array(
<?php
$count=0;
foreach($this->tableSchema->columns as $column) {
    if(++$count==7) {
        echo "        /*\n";
    }

    if($column->type == 'boolean') {
        echo "        array(
            'class' => 'MyBooleanColumn',
            'name' => '".$column->name."',\n";
        echo "        ),\n";
    } else {
        echo "        '".$column->name."',\n";
    }
}
if($count>=7) {
    echo "        */\n";
}
?>
        array(
            'class' => 'MyButtonColumn',
        ),
    ),
));
