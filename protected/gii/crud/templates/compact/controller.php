<?php
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass . "\n"; ?>
{
    public $model = '<?php echo preg_replace('/Base$/', '', substr($this->model, strrpos($this->model, '.'))); ?>';

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'users' => array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    public function actions()
    {
        return array(
            'index' => 'application.components.backend.actions.RedirectToAdmin',
            'admin' => 'application.components.backend.actions.Admin',
            'create' => 'application.components.backend.actions.Create',
            'update' => 'application.components.backend.actions.Update',
            'delete' => 'application.components.backend.actions.Delete',
        );
    }
}
