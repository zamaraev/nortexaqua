<?php

/**
 * Класс модели для таблицы "backend_users".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @package Models
 * @author  Dmitriy Neshin <just.neshin@gmail.com>
 */
class BackendUsersBase extends MyActiveRecord
{
    /**
     * Правила.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('login, password, created_at', 'required'),
            array('login, password', 'length', 'max' => 20),
            array('status, updated_at', 'safe'),
            array('id, login, password, status, created_at, updated_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Связи.
     *
     * @return array
     */
    public function relations()
    {
        return array(
        );
    }
}
