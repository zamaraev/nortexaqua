<?php

class FrontendForgotForm extends CFormModel
{
    public $username;

    public function rules()
    {
        return array(
            array('username', 'required'),
            array('username', 'email'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'username' => 'E-mail',
        );
    }

    public function sendMail()
    {
        $res = true;

        /** @var $client Clients */
        $client = Clients::model()->findByAttributes(array('email' => $this->username));
        if ($client !== null) {
            $client->setForgotPasswordKey();

            $absoluteUrl = Yii::app()->createAbsoluteUrl('/clients/default/getNewPassword', array('key' => $client->forgot_password_key));

            $res = MyMailer::getInstance()
                ->setTo($client->email)
                ->setSubject('Восстановление пароля')
                ->setText('Здравствуйте. Вы запросили восстановление пароля. Пройдите по ссылке: ' . $absoluteUrl)
                ->send();
        }

        return $res;
    }
}
