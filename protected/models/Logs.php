<?php

/**
 * Класс модели для таблицы "logs".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $device_id
 * @property string $type
 * @property string $description
 * @property string $created_at
 *
 */
class Logs extends MyActiveRecord
{
    /**
     *
     *
     * @param string  $className
     * @return self
     */
    public static function model( $className = __CLASS__ ) {
        return parent::model( $className );
    }

    /**
     * Правила.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('created_at', 'required'),
            array('order_id, device_id', 'numerical', 'integerOnly' => true),
            array('type', 'length', 'max' => 255),
            array('description', 'safe'),
            array('id, order_id, device_id, type, description, created_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Связи.
     *
     * @return array
     */
    public function relations()
    {
        return array(
            'order' => array( self::BELONGS_TO, 'Orders', 'order_id' ),
            'device' => array( self::BELONGS_TO, 'SettingsDevices', 'device_id' ),
        );
    }

    public function defaultScope()
    {
        return array(
            'order' => $this->getTableAlias(true, false) . '.created_at DESC',
        );
    }
}
