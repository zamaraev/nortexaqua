<?php

class BackendLoginForm extends CFormModel
{
    public $username;
    public $password;
    public $rememberMe;

    private $_identity;

    public function rules()
    {
        return array(
            array('username, password', 'required'),
            array('rememberMe', 'boolean'),
            array('password', 'authenticate'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'username' => 'Логин',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        );
    }

    public function authenticate()
    {
        if (!$this->hasErrors()) {
            $this->_identity = new MyBackendUserIdentity($this->username, $this->password);
            if (!$this->_identity->authenticate()) {
                $this->addError('password', 'Неверный логин или пароль.');
            }
        }
    }

    public function login()
    {
        if ($this->_identity === null) {
            $this->_identity = new MyBackendUserIdentity($this->username, $this->password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === MyBackendUserIdentity::ERROR_NONE) {
            $duration = 3600 * 24 * 30;
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        } else {
            return false;
        }
    }
}
