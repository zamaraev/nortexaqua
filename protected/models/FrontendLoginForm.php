<?php

class FrontendLoginForm extends CFormModel
{
    public $username;
    public $password;
    public $rememberMe;

    private $_identity;

    public function rules()
    {
        return array(
            array('username, password', 'required'),
//            array('username', 'email'),
            array('rememberMe', 'boolean'),
            array('password', 'authenticate'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'username' => 'Логин',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        );
    }

    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_identity = new MyFrontendUserIdentity($this->username, $this->password);
            if (!$this->_identity->authenticate()) {
                $labels = $this->attributeLabels();
                $this->addError('password', sprintf('Неверный %s или %s.', mb_strtolower($labels['username'], 'utf-8'), mb_strtolower($labels['password'], 'utf-8')));
            }
        }
    }

    public function login()
    {
        if ($this->_identity === null) {
            $this->_identity = new MyFrontendUserIdentity($this->username, $this->password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === MyFrontendUserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0;
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        } else {
            return false;
        }
    }
}
