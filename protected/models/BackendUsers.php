<?php

class BackendUsers extends BackendUsersBase
{
    /**
     * @static
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
