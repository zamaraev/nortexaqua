<?php

$urlManager = array(
    'class' => 'application.components.UrlManager',
    'urlFormat' => 'path',
    'urlSuffix' => '.html',
    'showScriptName' => false,
    'rules' => array(
        '<language:(ru|ua)>/<module:\w+>/<controller:\w+>/<id:\d+>/<action:\w+>' => '<module>/<controller>/<action>',
        '<language:(ru|ua)>/<module:\w+>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/view',
        '<language:(ru|ua)>/<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
        '<language:(ru|ua)>/<module:\w+>/<controller:\w+>' => '<module>/<controller>',
        '<language:(ru|ua)>/<module:\w+>' => '<module>',
    ),

);

return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'),
    array(
        'defaultController' => 'site/index',
        'import' => array(
            'application.components.frontend.*',
            'application.modules.settings.models.*',
            'application.modules.settings.models.*',


        ),
        'components' => array(
            'request'=>array(
                'enableCookieValidation'=>true,

            ),
            'urlManager' => $urlManager,
            'user' => array(
                'class' => 'application.web.auth.MyFrontendWebUser',
                'allowAutoLogin' => true,
                'loginUrl' => array('clients/default/login'),
            ),

        ),
        'params'=>array(
            'languages'=>array('ru'=>'Русский', 'ua'=>'Українська'),
        ),
    )
);
