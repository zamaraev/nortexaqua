<?php

date_default_timezone_set('Europe/Moscow');

function p($var)
{
    print_r($var);
}

function d($var, $depth = 10, $highlight = true)
{
    CVarDumper::dump($var, $depth, $highlight);
}

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'MyCMS',
    'preload' => array('log'),
    'sourceLanguage'=>'en',
    'language'=>'ru',
    'import' => array(
        // 'application.base.*',
        'application.components.*',
        'application.db.ar.*',
        'application.models.*',
        'application.web.*',
        'application.web.helpers.*',
        'application.web.widgets.*',
        'application.web.widgets.pagers.*',
        'application.modules.news.models.*',
        'application.modules.products.models.*',
        'application.modules.contacts.models.*',
        'application.modules.blogposts.models.*',





    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '123123123',
            'generatorPaths' => array(
                'application.gii',
            ),
            'ipFilters' => array('127.0.0.1'),
        ),
        'settings',
        'news',
        'products',
        'contacts',
        'blogposts',



    ),
    'behaviors' => array(
        'runEnd' => array(
            'class' => 'application.behaviors.WebApplicationEndBehavior',
        ),
    ),
    'components' => array(
        'themeManager' => array(
            'class' => 'application.web.MyThemeManager',
        ),
        'cache' => array(
            'class' => 'system.caching.CFileCache',
        ),
        'clientScript' => array(
            'class' => 'application.web.MyClientScript',
        ),
        'errorHandler' => array(
            'class' => 'application.base.MyErrorHandler',
            'errorAction' => 'site/error',
        ),
        'messages' => array(
            'onMissingTranslation' => array('MyEventHandler', 'handleMissingTranslation'),
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
//                array(
//                    'class' => 'CWebLogRoute',
//                ),
            ),
        ),
    ),
    'params' => array(
        'adminEmail' => 'send@nortexaqua.ru',
        'myMailer' => array(
            'username' => '',
            'password' => '',
        ),
    ),
);
