<?php

$mainConfig = require(dirname(__FILE__) . '/main.php');

$baseUrl = $mainConfig['components']['request']['hostInfo'] . $mainConfig['components']['request']['baseUrl'];

return CMap::mergeArray(
    $mainConfig,
    array(
        'defaultController' => 'news/news/admin',
        'import' => array(
            'application.components.backend.*',
            'application.modules.news.models.*',
            'application.modules.products.models.*',
            'application.modules.contacts.models.*',
            'application.modules.blogposts.models.*',



        ),
        'components' => array(
            'request' => array(
                'scriptUrl' => $backend['scriptUrl'],
            ),
            'clientScript' => array(
                'scriptMap' => array(
                    'jquery-ui.css' => $baseUrl . '/css/backend/widgets/jui/themes/custom/theme.css',
                ),
                'corePackages' => array_merge(
                    require(YII_PATH.'/web/js/packages.php'),
                    array(
                        'less' => array(
                            'baseUrl' => $baseUrl . '/',
                            'js' => array('js/less-1.3.0.min.js'),
                            'depends' => array('jquery'),
                        ),
                    )
                ),
            ),
            'authManager' => array(
                'class' => 'application.web.auth.MyDbAuthManager',
                'itemTable' => 'auth_item',
                'itemChildTable' => 'auth_item_child',
                'assignmentTable' => 'auth_assignment',
            ),
            'user' => array(
                'class' => 'application.web.auth.MyBackendWebUser',
                'allowAutoLogin' => true,
                'loginUrl' => array('site/login'),
            ),
        ),
    )
);
