<?php

$config = require(dirname(__FILE__) . '/main.php');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => $config['name'],
    'preload' => array('log'),
    'language' => $config['language'],
    'import' => array(
        'application.components.*',
        'application.db.ar.*',
        'application.components.db.*',
        'application.models.*',
        'application.modules.settings.models.*',
        'application.modules.news.models.*',
        'application.modules.products.models.*',
        'application.modules.contacts.models.*',





    ),
    'components' => array(
        'db' => $config['components']['db'],
    ),
    'modules' => array(
        'settings',
        'news',
        'products',
        'contacts',

    ),
    'commandMap' => array(
        'migrate' => array(
            'class' => 'application.extensions.EMigrate.EMigrateCommand',
            'migrationPath' => 'application.migrations',
            'migrationTable' => 'tbl_migration',
            'applicationModuleName' => 'core',
            'modulePaths' => array(
                'settings' => 'application.modules.settings.migrations',
                'news' => 'application.modules.news.migrations',
                'products' => 'application.modules.products.migrations',
                'contacts' => 'application.modules.contacts.migrations',

            ),
            'connectionID' => 'db',
            'templateFile' => 'application.components.db.Template',
        ),
    ),
);
