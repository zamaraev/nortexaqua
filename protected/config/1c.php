<?php

return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'),
    array(
        'theme' => null,
        'import' => array(
            'application.components.1c.*',
            'application.modules.dicts.models.*',
        ),
        'defaultController' => null,
        'components' => array(
            'urlManager' => array(
                'class' => 'MyUrlManager',
                'urlFormat' => 'get',
            ),
        ),
    )
);
